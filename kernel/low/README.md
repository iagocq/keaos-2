For a lack of a better way to load low (0x0500) code with the bootloader / early kernel (while using ld quirks),
this code should be compiled and coded into a C source file in the kernel and then copied to where it belongs by the early kernel.
