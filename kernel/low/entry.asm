[bits 32]
section .first

extern enter_real_mode
extern enter_protected_mode
extern restore_idt

low_entry:
    pop     eax     ; return address
    mov     [save_esp], esp
    mov     [save_ebp], ebp
    mov     [save_ret], eax

    call    enter_real_mode

[bits 16]
    pop     eax     ; int number
    mov     [int_number], al
    jmp     0:.clear_pipeline

.clear_pipeline:
    popad
    db      0xCD    ; int instruction
int_number: db 0    ; int number
    cli

    mov     esp, gpregs_save.end

    pushad  ; save gp registers + eflags for caller access
    pushfd  ;

    mov     esp, [save_esp]

    call    enter_protected_mode
[bits 32]
    call    restore_idt

    mov     eax, eflags_save
    mov     ebp, [save_ebp]
    jmp     [save_ret]

save_ret:       dd 0
save_ebp:       dd 0
save_esp:       dd 0

dd 0x87654321
eflags_save:    dd 0
gpregs_save:    times 8 dd 0
.end:
dd 0x12345678
