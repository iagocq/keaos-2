/**
 * @file multiboot2.h
 * @brief Multiboot2 related structures.
 */

#pragma once

#include <stdint.h>

#define MB2_HEADER_MAGIC 0xe85250d6

/**
 * @enum mb2_tag_type
 * @brief Enumeration of MB2 tag types.
 */
typedef enum mb2_tag_type {
    MB2_TAG_TYPE_END,
    MB2_TAG_TYPE_CMDLINE,
    MB2_TAG_TYPE_BOOT_LOADER_NAME,
    MB2_TAG_TYPE_MODULE,
    MB2_TAG_TYPE_BASIC_MEMINFO,
    MB2_TAG_TYPE_BOOTDEV,
    MB2_TAG_TYPE_MMAP,
    MB2_TAG_TYPE_VBE,
    MB2_TAG_TYPE_FRAMEBUFFER,
    MB2_TAG_TYPE_ELF_SECTIONS,
    MB2_TAG_TYPE_APM,
    MB2_TAG_TYPE_EFI32,
    MB2_TAG_TYPE_EFI64,
    MB2_TAG_TYPE_SMBIOS,
    MB2_TAG_TYPE_ACPI_OLD,
    MB2_TAG_TYPE_ACPI_NEW,
    MB2_TAG_TYPE_NETWORK,
    MB2_TAG_TYPE_EFI_MMAP,
    MB2_TAG_TYPE_EFI_BS,
    MB2_TAG_TYPE_EFI32_IH,
    MB2_TAG_TYPE_EFI64_IH,
    MB2_TAG_TYPE_LOAD_BASE_ADDR
} mb2_tag_type_t;

/**
 * @enum memory_type
 * @brief Type of mapped memory returned by MB2.
 */
typedef enum memory_type {
    MB2_MEM_AVAILABLE = 1, ///< Memory is available for use
    MB2_MEM_RESERVED,      ///< Memory is reserved, thus it should not be used
    MB2_MEM_ACPI_RECLAIMABLE,
    MB2_MEM_NVS,
    MB2_MEM_BADRAM
} memory_type_t;

/**
 * @struct mb2_mmap_entry
 * @brief Mapped memory entry in mb2_tag_mmap.
 */
typedef struct mb2_mmap_entry {
    uint64_t      addr; ///< Address of mapped memory
    uint64_t      len;  ///< Length of mapped memory
    memory_type_t type; ///< Type of mapped memory
    uint32_t      zero;
} __attribute__((__packed__)) mb2_mmap_entry_t;

/**
 * @struct mb2_tag
 * @brief MB2 tag generic header. Every MB2 tag contains this header.
 */
typedef struct mb2_tag {
    mb2_tag_type_t type; ///< Type of the tag
    uint32_t       size; ///< Size of the tag header
} __attribute__((__packed__)) mb2_tag_t;

/**
 * @struct mb2_tag_mmap
 * @brief Memory map MB2 tag, MB2_TAG_TYPE_MMAP
 */
typedef struct mb2_tag_mmap {
    mb2_tag_t             tag;
    uint32_t              entry_size;    ///< Size of this entry, guaranteed to be multiple of 8
    uint32_t              entry_version; ///< Version of the entry
    struct mb2_mmap_entry entries[];     ///< Array of mb2_mmap_entry entries
} __attribute__((__packed__)) mb2_tag_mmap_t;

/************************************************
 * PUBLIC FUNCTIONS                             *
 ************************************************/

void init_mb2(uint32_t mb2_struct_pointer);
