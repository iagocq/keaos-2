#pragma once

#include <assert.h>
#include <stdint.h>

#define PCI_CONFIG_ADDRESS 0xcf8
#define PCI_CONFIG_DATA    0xcfc

typedef union pci_config_address {
    struct __attribute__((__packed__)) {
        uint8_t offset;     // Offset to get from configuration address
        uint8_t func : 3;   // Choose a specific function from device (if supported)
        uint8_t device : 5; // Device number
        uint8_t bus;        // Bus number
        uint8_t rv0 : 7;    // Reserved
        uint8_t eb : 1;     // Enable bit
    };
    uint32_t data;
} pci_config_address_t;
static_assert(sizeof(pci_config_address_t) == 4, "pci_config_address_t has wrong size");

typedef union pci_device_header {
    struct __attribute__((__packed__)) {
        uint16_t vendor_id;
        uint16_t device_id;
        uint16_t command;
        uint16_t status;
        uint8_t  revision_id;
        uint8_t  prog_if;
        uint8_t  subclass;
        uint8_t  class_code;
        uint8_t  cache_line_size;
        uint8_t  latency_timer;
        uint8_t  header_type;
        uint8_t  bist;
    };
    uint32_t data[4];
} pci_device_header_t;
static_assert(sizeof(pci_device_header_t) == 16, "pci_device_header_t has wrong size");

typedef union pci_device_generic_info {
    struct __attribute__((__packed__)) {
        uint32_t bars[6];
        uint32_t cc_ptr;
        uint16_t subsystem_vendor_id;
        uint16_t subsystem_id;
        uint32_t erb_addr;
        uint8_t  capabilities_ptr;
        uint8_t  rv0[7];
        uint8_t  interrupt_line;
        uint8_t  interrupt_pin;
        uint8_t  min_grant;
        uint8_t  max_latency;
    };
    uint32_t data[12];
} pci_device_generic_info_t;
static_assert(sizeof(pci_device_generic_info_t) == 48, "pci_device_generic_info_t has wrong size");

typedef union pci_device {
    struct __attribute__((__packed__)) {
        pci_device_header_t header;
        union {
            pci_device_generic_info_t info_generic;
        };
    };
    uint32_t data[16];
} pci_device_t;
static_assert(sizeof(pci_device_t) == 64, "pci_device_t has wrong size");

typedef struct ll_pci_device_node {
    struct ll_pci_device_node *next;
    pci_device_t               device;
    uint8_t                    bus;
    uint8_t                    slot;
    uint8_t                    func;
} ll_pci_device_node_t;

uint16_t                  enumerate_pci();
pci_device_header_t       pci_get_device_header(uint8_t bus, uint8_t device, uint8_t func);
pci_device_generic_info_t pci_get_device_generic_info(uint8_t bus, uint8_t device, uint8_t func);
pci_device_t              pci_get_device(uint8_t bus, uint8_t device, uint8_t func);
uint16_t                  load_pci_devices();
uint8_t                   unload_pci_devices();

uint16_t pci_device_count;
