#pragma once

#include <types.h>

/*
    The disk primary disk buffer is the first buffer that read data is copied into (by the bios).
    As such, it should be entirely contained inside the first real mode segment of memory
   (<=0xffff). That restriction applies only because I'm too lazy to properly use the DS segment
   register, otherwise 0x7ffff should be accessible. 0x0500 is the start of a big enough contiguous
   free memory area. Some code and data should be put there and reserved first, because the BIOS
   expects real mode code and data not to be located after the first MiB.
*/
#define PRIMARY_DISK_BUFFER 0x0700

typedef struct __attribute__((__packed__)) partition {
    uint8_t  bootable;
    uint8_t  start_addr[3];
    uint8_t  type;
    uint8_t  end_addr[3];
    uint32_t lba_start;
    uint32_t sectors_n;
} partition_t;
CASSERT(sizeof(partition_t) == 16, disk_h);

typedef struct __attribute__((__packed__)) mbr {
    uint8_t     bootstrap[440];
    uint32_t    disk_id;
    uint16_t    a;
    partition_t partitions[4];
    uint16_t    signature;
} mbr_t;
CASSERT(sizeof(mbr_t) == 512, disk_h);

typedef struct __attribute__((__packed__)) fat32_bpb {
    uint16_t bytes_per_lsector;
    uint8_t  lsectors_per_cluster;
    uint16_t reserved_lsectors;
    uint8_t  num_fat;
    uint16_t root_entries;
    uint16_t old_total_lsectors;
    uint8_t  media_type;
    uint16_t old_lsectors_per_fat;

    uint16_t psectors_per_track;
    uint16_t num_heads;
    uint32_t hidden_sectors;
    uint32_t total_lsectors;

    uint32_t lsectors_per_fat;
    uint16_t mirror_flags;
    uint16_t version;
    uint32_t root_cluster;
    uint16_t fs_info_lsector;
    uint16_t bs_backup_lsector;
    uint8_t  reserved_1[12];
    uint8_t  old_pdrive;
    uint8_t  filler;
    uint8_t  old_boot_signature;

    uint32_t volume_id;
    uint8_t  volume_label[11];
    uint8_t  fs_type[8];

} fat32_bpb_t;
CASSERT(sizeof(fat32_bpb_t) == 79, disk_h);

typedef struct __attribute__((__packed__)) fat32_bootsector {
    uint8_t     jmp[3];
    uint8_t     oem_name[8];
    fat32_bpb_t fat32_bpb;
    uint8_t     boot_code[419];
    uint8_t     old_pdrive;
    uint16_t    boot_signature;
} fat32_bootsector_t;
CASSERT(sizeof(fat32_bootsector_t) == 512, disk_h);

typedef struct __attribute__((__packed__)) mbr_bootsector {
    union {
        uint8_t boot_code[440];
        struct __attribute__((__packed__)) {
            uint8_t  boot_code_1[218];
            uint16_t zero;
            uint8_t  pdrive;
            uint8_t  seconds;
            uint8_t  minutes;
            uint8_t  hours;
            uint8_t  boot_code_2[216];
        };
    };
    union {
        uint8_t eboot_code[6];
        struct __attribute__((__packed__)) {
            uint32_t signature;
            uint16_t copy_protected;
        };
    };
    partition_t partitions[4];
    uint16_t    boot_signature;
} mbr_bootsector_t;
CASSERT(sizeof(mbr_bootsector_t) == 512, disk_h);

typedef enum { FAT32_BOOTSECTOR, MBR_BOOTSECTOR } bootsector_type;

typedef struct bootsector {
    bootsector_type bs_type;
    union {
        fat32_bootsector_t fat32_bs;
        mbr_bootsector_t   mbr_bs;
    };
} bootsector_t;

typedef struct disk {
    uint8_t      drive_number;
    uint16_t     bytes_per_sector;
    bootsector_t bootsector;
} disk_t;

/**
 * Save the BIOS drive number for later usage with disk manipulation functions
 */
void init_disk(uint8_t drive_number);
int  read_sectors(uint16_t sectors,
                  void *   memory,
                  uint32_t start_sector_low,
                  uint32_t start_sector_high);
int  read_sectors_offset(uint16_t sectors,
                         void *   memory,
                         uint32_t start_sector_low,
                         uint32_t start_sector_high,
                         uint32_t start,
                         uint32_t size);

disk_t disk;
