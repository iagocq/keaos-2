#pragma once

#include <types.h>

typedef struct vbe2_far_ptr {
    union {
        uint32_t linear;
        void *   ptr;
        struct {
            uint16_t offset;
            uint16_t segment;
        } __attribute__((packed));
    };
} __attribute__((packed)) vbe2_far_ptr;

typedef struct vbe2_info {
    uint8_t      signature[4];
    uint16_t     version;
    vbe2_far_ptr oem_string;
    uint8_t      capabilities[4];
    vbe2_far_ptr video_modes;
    uint16_t     total_mem;
    uint16_t     software_rev;
    vbe2_far_ptr vendor_string;
    vbe2_far_ptr model_name;
    vbe2_far_ptr product_rev;
    uint8_t      reserved[222];
    uint8_t      oem_data[256];
} __attribute__((packed)) vbe2_info_t;

typedef struct vbe2_mode_info {
    uint16_t     attributes;
    uint8_t      win_a, win_b;
    uint16_t     granularity;
    uint16_t     winsize;
    uint16_t     segment_a, segment_b;
    vbe2_far_ptr real_fct;
    uint16_t     pitch;

    uint16_t w, h;
    uint8_t  wchar, ychar, planes, bpp, banks;
    uint8_t  memory_model, bank_size, image_pages;
    uint8_t  reserved_0;

    uint8_t r_mask, r_pos;
    uint8_t g_mask, g_pos;
    uint8_t b_mask, b_pos;
    uint8_t rsv_mask, rsv_pos;
    uint8_t directcolor_attr;

    void *   framebuffer;
    uint32_t reserved_1;
    uint16_t reserved_2;
} __attribute__((packed)) vbe2_mode_info_t;

int      vbe2_get_bios_info(vbe2_info_t *vbe_info);
int      vbe2_get_mode_info(uint16_t mode, vbe2_mode_info_t *vbe_mode_info);
uint16_t vbe2_get_best_mode(uint16_t w, uint16_t h, uint8_t bpp, vbe2_info_t *vbe_info);
int      vbe2_set_mode(uint16_t mode);
void *   vbe2_find_and_set_mode(uint16_t w, uint16_t h, uint8_t bpp, vbe2_info_t *vbe_info);
