/**
 * @file descriptor_tables.h
 * @brief X86 descriptor tables related structs.
 *
 * The GDT and the IDT are descriptor tables. They are arrays of flags and bit values describing the
 * operation of either the segmentation system (in the case of the GDT), or the interrupt vector
 * table (IDT).
 */

#pragma once

#include <stdint.h>

/**
 * @struct gdt_entry
 * @brief This structure contains the value of one GDT entry.
 */
typedef struct gdt_entry {
    uint16_t limit_low;   ///< The lower 16 bits of the limit.
    uint16_t base_low;    ///< The lower 16 bits of the base.
    uint8_t  base_middle; ///< The next 8 bits of the base.
    uint8_t  access;      ///< bit 7   → Is segment present?\n
                          ///< bit 6:5 → Privilege level (ring 0 or 3)\n
                          ///< bit 4   → Descriptor type\n
                          ///< bit 3:0 → Segment type (code or data)
    uint8_t granularity;  ///< bit 7   → Granularity (0 = 1 byte, 1 = 1KiB)\n
                          ///< bit 6   → Operand size (0 = 16bit, 1 = 32bit)\n
                          ///< bit 5   → Always 0\n
                          ///< bit 4   → Available for system use (always 0)\n
                          ///< bit 3:0 → Segment length
    uint8_t base_high;    ///< The last 8 bits of the base.
} __attribute__((__packed__)) gdt_entry_t;

/**
 * @struct idt_entry
 * @brief This structure contains the value of one IDT entry.
 */
typedef struct idt_entry {
    uint16_t base_lo; ///< The lower 16 bits of the address to jump to when this interrupt fires.
    uint16_t sel;     ///< Kernel segment selector.
    uint8_t  always0; ///< This must always be zero.
    uint8_t  flags; ///< bit 7   → Present. If not set, means "Interrupt not handled" exception.\n
                    ///< bit 6:5 → DPL. Privilege level where we expect to be called from.\n
                    ///< bit 4:0 → Always 00110
    uint16_t base_hi; ///< The upper 16 bits of the address to jump to.
} __attribute__((__packed__)) idt_entry_t;

/**
 * @struct descriptor_ptr
 * @brief A struct describing a pointer to an array of idt_entry or gdt_entry. This
 * is in a format suitable for giving to 'lidt' or 'lgdt'.
 */
typedef struct descriptor_ptr {
    uint16_t limit; ///< Size of the array minus 1
    uint32_t base;  ///< The address of the first element in our descriptor array.
} __attribute__((__packed__)) descriptor_ptr_t;

/**
 * \brief Initializes GDT and IDT.
 */
void init_descriptor_tables();
