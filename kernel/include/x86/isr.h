/**
 * @file isr.h
 * @brief Interface and structures for high level interrupt service routines.
 */

#pragma once

#include <stdint.h>

#define low_16(addr)  (uint16_t)((addr) &0xffff)
#define high_16(addr) (uint16_t)(((addr) >> 16) & 0xffff)

/**
 * @struct registers
 * @brief Struct containing x86 registers. Used in @ref isr_handler
 * @see isr_handler
 */
typedef struct registers {
    uint32_t gs, fs, es, ds;
    uint32_t edi, esi, ebp, useless, ebx, edx, ecx, eax; /* Pushed by pusha. */
    uint32_t int_no, err_code;         /* Interrupt number and error code (if applicable) */
    uint32_t eip, cs, eflags, esp, ss; /* Pushed by the processor automatically */
} registers_t;

// Enables registration of callbacks for interrupts or IRQs.
// For IRQs, to ease confusion, use the #defines above as the
// first parameter.
typedef void (*isr_t)(registers_t *);
void register_interrupt_handler(uint8_t n, isr_t handler);
void enable_irq();
void enable_interrupts();
void disable_interrupts();

typedef enum cpu_exception {
    EXCEPT_DIVISION_BY_ZERO         = 0,
    EXCEPT_DEBUG                    = 1,
    EXCEPT_NMI                      = 2,
    EXCEPT_BREAKPOINT               = 3,
    EXCEPT_OVERFLOW                 = 4,
    EXCEPT_OUT_OF_BOUNDS            = 5,
    EXCEPT_INVALID_OPCODE           = 6,
    EXCEPT_DEV_NOT_AVAILABLE        = 7,
    EXCEPT_DOUBLE_FAULT             = 8,
    EXCEPT_COPROCESSOR_SO           = 9,
    EXCEPT_INVALID_TSS              = 10,
    EXCEPT_SEGMENT_NOT_PRESENT      = 11,
    EXCEPT_STACK_FAULT              = 12,
    EXCEPT_GENERAL_PROTECTION_FAULT = 13,
    EXCEPT_PAGE_FAULT               = 14,
    EXCEPT_X87_FP                   = 16,
    EXCEPT_ALIGNMENT_CHECK          = 17,
    EXCEPT_MACHINE_CHECK            = 18,
    EXCEPT_SIMD_FP                  = 19,
    EXCEPT_VIRTUALIZATION           = 20,
    EXCEPT_SECURITY                 = 30
} cpu_exception_t;

extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();

/* IRQ definitions */
extern void irq0();
extern void irq1();
extern void irq2();
extern void irq3();
extern void irq4();
extern void irq5();
extern void irq6();
extern void irq7();
extern void irq8();
extern void irq9();
extern void irq10();
extern void irq11();
extern void irq12();
extern void irq13();
extern void irq14();
extern void irq15();

#define IRQ0  32
#define IRQ1  33
#define IRQ2  34
#define IRQ3  35
#define IRQ4  36
#define IRQ5  37
#define IRQ6  38
#define IRQ7  39
#define IRQ8  40
#define IRQ9  41
#define IRQ10 42
#define IRQ11 43
#define IRQ12 44
#define IRQ13 45
#define IRQ14 46
#define IRQ15 47
