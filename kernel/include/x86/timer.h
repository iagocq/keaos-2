/**
 * @file timer.h
 * @brief Defines the interface for all PIT-related functions.
 */

#pragma once

#include <stdint.h>

void init_timer(uint32_t freq);
