/**
 * @file screen.h
 * @brief Interface and structures for basic BIOS VGA text mode.
 */

#pragma once

#include <stdint.h>

#include <keaos/kprintf.h>

#define VIDEO_ADDRESS  0xC03FC000
#define MAX_ROWS       25
#define MAX_COLS       80
#define WHITE_ON_BLACK 0x0f
#define RED_ON_WHITE   0xf4

/* Screen I/O ports */
#define REG_SCREEN_CTRL 0x3d4
#define REG_SCREEN_DATA 0x3d5

#define VGA_COLOR_BLACK   0x00
#define VGA_COLOR_BLUE    0x01
#define VGA_COLOR_GREEN   0x02
#define VGA_COLOR_CYAN    0x03
#define VGA_COLOR_RED     0x04
#define VGA_COLOR_MAGENTA 0x05
#define VGA_COLOR_GRAY    0x07
#define VGA_COLOR_YELLOW  0x0e
#define VGA_COLOR_WHITE   0x0f

/**
 * @struct screen
 * @brief Holds current screen data.
 */
typedef struct screen {
    uint8_t   cur_x, cur_y;
    uint8_t   width, height;
    uint16_t  current_attributes;
    uint16_t *video; ///< Video memory.
} screen_t;

void clear_screen();
void init_screen();
void vga_putchar(char chr);
void vga_kputchar_handler(kprintf_state_t *state);
