#pragma once

typedef enum kprintf_color {
    KPRINTF_COLOR_INVALID,
    KPRINTF_COLOR_RED,
    KPRINTF_COLOR_GREEN,
    KPRINTF_COLOR_BLUE,
    KPRINTF_COLOR_CYAN,
    KPRINTF_COLOR_MAGENTA,
    KPRINTF_COLOR_YELLOW,
    KPRINTF_COLOR_WHITE,
    KPRINTF_COLOR_GRAY,
    KPRINTF_COLOR_BLACK,
} kprintf_color_t;

typedef struct kprintf_state {
    char current_char;

    kprintf_color_t foreground_color;
    kprintf_color_t background_color;
} kprintf_state_t;

typedef void (*kputchar_handler_t)(kprintf_state_t *);

void register_kputchar(kputchar_handler_t handler);
void kprintf(const char *format, ...) __attribute__((format(printf, 1, 2)));
