/**
 * @file string-utils.h
 * @brief Miscellaneous string utilities.
 */

#pragma once

char *itoa(int num, char *str, int base);
