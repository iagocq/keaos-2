#pragma once
#include <stddef.h>
#include <stdint.h>

typedef struct llmalloc_block_h {
    /* Pointer to the next block. Is NULL if it's the last one */
    struct llmalloc_block_h *next_block;

    /* Size in bytes of this block (including the header) */
    size_t size;

    /* Set to 1 if this block is free */
    uint8_t free;
} llmalloc_block_h_t;

typedef struct llmalloc_control {
    /* Pointer to the start of the linked list */
    llmalloc_block_h_t *first;

    /* Pointer to where the first block header should be located */
    void *start_mem;
} llmalloc_control_t;

void    init_llmalloc(void *start, size_t max_bytes);
void *  llmalloc(size_t bytes);
uint8_t llfree(void *mem);
uint8_t llrealloc(void *mem, size_t bytes);
size_t  free_bytes();
size_t  available_bytes();
void *  llalign(size_t alignment, size_t bytes);

size_t ll_total_bytes;
