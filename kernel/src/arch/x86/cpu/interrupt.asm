[bits 32]
; Defined in isr.c
[extern isr_handler]
[extern irq_handler]

; ISR handler
isr_common_stub:
    ; Save CPU state
    pusha
    xor eax, eax

    mov ax, ds
    push eax        ; Save data segment descriptor
    mov ax, es
    push eax        ; likewise for the other SDs
    mov ax, fs
    push eax
    mov ax, gs
    push eax

    mov ax, 0x10    ; Kernel segment descriptor
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    push esp        ; registers_t *r

    ; Call C handler
    cld
    call isr_handler

    ; Restore state
    pop eax

    pop eax
    mov gs, ax
    pop eax
    mov fs, ax
    pop eax
    mov es, ax
    pop eax
    mov ds, ax

    popa
    add esp, 8      ; Cleans up the error code and pushed ISR number
    iret

; IRQ handler
irq_common_stub:
    ; Save CPU state
    pusha 

    xor eax, eax

    mov ax, ds
    push eax        ; Save data segment descriptor
    mov ax, es
    push eax        ; likewise for the other SDs
    mov ax, fs
    push eax
    mov ax, gs
    push eax

    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    push esp

    ; Call C handler
    cld
    call irq_handler

    ; Restore state
    pop ebx

    pop ebx
    mov gs, bx
    pop ebx
    mov fs, bx
    pop ebx
    mov es, bx
    pop ebx
    mov ds, bx

    popa
    add esp, 8 ; Cleans up the error code and pushed IRQ number
    iret

; Create a handler for every interrupt, for identifying purposes

; Not every interrupt pushes an error code, so make a dummy one for the ones
; that do not push

; Non-exception interrupt handler macro
%macro isr 1
    global isr%1
isr%1:
    push byte 0
    push byte %1
    jmp isr_common_stub
%endmacro
; Exception interrupt handler macro
%macro isr_e 1
    global isr%1
isr%1:
    push byte %1
    jmp isr_common_stub
%endmacro

; Generic IRQ handler macro
%macro irq 1
    global irq%1
irq%1:
    push byte %1
    push byte %1+32
    jmp irq_common_stub
%endmacro

; 0: Divide by 0 exception
isr 0
; 1: Debug exception
isr 1
; 2: Non maskable interrupt exception
isr 2
; 3: Int 3 exception (Breakpoint)
isr 3
; 4: INTO exception
isr 4
; 5: Out of bounds exception
isr 5
; 6: Invalid Opcode Exception
isr 6
; 7: Coprocessor Not Available Exception
isr 7
; 8: Double Fault Exception (With Error Code!)
isr_e 8
; 9: Coprocessor Segment Overrun Exception
isr 9
; 10: Bad TSS Exception (With Error Code!)
isr_e 10
; 11: Segment Not Present Exception (With Error Code!)
isr_e 11
; 12: Stack Fault Exception (With Error Code!)
isr_e 12
; 13: General Protection Fault Exception (With Error Code!)
isr_e 13
; 14: Page Fault Exception (With Error Code!)
isr_e 14
; 15: Reserved Exception
isr 15
; 16: Floating Point Exception
isr 16
; 17: Alignment Check Exception
isr 17
; 18: Machine Check Exception
isr 18
; 19-31: Reserved
isr 19
isr 20
isr 21
isr 22
isr 23
isr 24
isr 25
isr 26
isr 27
isr 28
isr 29
isr 30
isr 31

irq 0
irq 1
irq 2
irq 3
irq 4
irq 5
irq 6
irq 7
irq 8
irq 9
irq 10
irq 11
irq 12
irq 13
irq 14
irq 15
