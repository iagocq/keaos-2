/**
 * @file descriptor_tables.c
 * @brief Initialises the GDT and IDT, and defines the default ISR and IRQ handler.
 */

#include <string.h>
#include <x86/descriptor_tables.h>
#include <x86/isr.h>
#include <x86/ports.h>

// Defined in descriptor_tables.asm
extern void gdt_flush(uint32_t gdt_pointer);
extern void idt_flush(uint32_t idt_pointer);

/************************************************
 * PRIVATE FUNCTIONS                            *
 ************************************************/

// Forward declarations
static void init_gdt();
static void init_idt();
static void gdt_set_gate(uint32_t idx, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran);
static void idt_set_gate(uint32_t idx, uint32_t base, uint16_t selector, uint8_t flags);

/**
 * We will initialise 5 gdt_entries. One is null segment and the other 4 will map the entire memory
 * range as user and kernel data and code. This is because we will use paging as memory management,
 * and GDT is mandatory on protected mode. As we are booting using multiboot2 protocol, the
 * bootloader will set up a GDT, but not tell us where it is, so to not accidently overwrite it
 * later, we must set a new GDT.
 */
gdt_entry_t      gdt_entries[5];
idt_entry_t      idt_entries[256];
descriptor_ptr_t gdt_ptr, idt_ptr;

/**
 * @brief Setup GDT. Map entire memory as user and kernel.
 */
static void init_gdt() {
    gdt_ptr.limit = (sizeof(gdt_entry_t) * 5) - 1;
    gdt_ptr.base  = (uint32_t) &gdt_entries;

    gdt_set_gate(0, 0, 0, 0, 0);                // Null segment
    gdt_set_gate(1, 0, 0xFFFFFFFF, 0x9A, 0xCF); // Code segment
    gdt_set_gate(2, 0, 0xFFFFFFFF, 0x92, 0xCF); // Data segment
    gdt_set_gate(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); // User mode code segment
    gdt_set_gate(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); // User mode data segment

    gdt_flush((uint32_t) &gdt_ptr);
}

/**
 * @brief Setup IDT.
 */
static void init_idt() {
    idt_ptr.limit = (sizeof(idt_entry_t) * 256) - 1;
    idt_ptr.base  = (uint32_t) &idt_entries;

    // Clean up memory just in case
    memset(&idt_entries, 0, sizeof(idt_entry_t) * 256);

    idt_set_gate(0, (uint32_t) isr0, 0x08, 0x8E);
    idt_set_gate(1, (uint32_t) isr1, 0x08, 0x8E);
    idt_set_gate(2, (uint32_t) isr2, 0x08, 0x8E);
    idt_set_gate(3, (uint32_t) isr3, 0x08, 0x8E);
    idt_set_gate(4, (uint32_t) isr4, 0x08, 0x8E);
    idt_set_gate(5, (uint32_t) isr5, 0x08, 0x8E);
    idt_set_gate(6, (uint32_t) isr6, 0x08, 0x8E);
    idt_set_gate(7, (uint32_t) isr7, 0x08, 0x8E);
    idt_set_gate(8, (uint32_t) isr8, 0x08, 0x8E);
    idt_set_gate(9, (uint32_t) isr9, 0x08, 0x8E);
    idt_set_gate(10, (uint32_t) isr10, 0x08, 0x8E);
    idt_set_gate(11, (uint32_t) isr11, 0x08, 0x8E);
    idt_set_gate(12, (uint32_t) isr12, 0x08, 0x8E);
    idt_set_gate(13, (uint32_t) isr13, 0x08, 0x8E);
    idt_set_gate(14, (uint32_t) isr14, 0x08, 0x8E);
    idt_set_gate(15, (uint32_t) isr15, 0x08, 0x8E);
    idt_set_gate(16, (uint32_t) isr16, 0x08, 0x8E);
    idt_set_gate(17, (uint32_t) isr17, 0x08, 0x8E);
    idt_set_gate(18, (uint32_t) isr18, 0x08, 0x8E);
    idt_set_gate(19, (uint32_t) isr19, 0x08, 0x8E);
    idt_set_gate(20, (uint32_t) isr20, 0x08, 0x8E);
    idt_set_gate(21, (uint32_t) isr21, 0x08, 0x8E);
    idt_set_gate(22, (uint32_t) isr22, 0x08, 0x8E);
    idt_set_gate(23, (uint32_t) isr23, 0x08, 0x8E);
    idt_set_gate(24, (uint32_t) isr24, 0x08, 0x8E);
    idt_set_gate(25, (uint32_t) isr25, 0x08, 0x8E);
    idt_set_gate(26, (uint32_t) isr26, 0x08, 0x8E);
    idt_set_gate(27, (uint32_t) isr27, 0x08, 0x8E);
    idt_set_gate(28, (uint32_t) isr28, 0x08, 0x8E);
    idt_set_gate(29, (uint32_t) isr29, 0x08, 0x8E);
    idt_set_gate(30, (uint32_t) isr30, 0x08, 0x8E);
    idt_set_gate(31, (uint32_t) isr31, 0x08, 0x8E);

    // Remap the irq table.
    outb(0x20, 0x11);
    outb(0xA0, 0x11);
    outb(0x21, 0x20);
    outb(0xA1, 0x28);
    outb(0x21, 0x04);
    outb(0xA1, 0x02);
    outb(0x21, 0x01);
    outb(0xA1, 0x01);
    outb(0x21, 0x0);
    outb(0xA1, 0x0);

    idt_set_gate(IRQ0, (uint32_t) irq0, 0x08, 0x8E);
    idt_set_gate(IRQ1, (uint32_t) irq1, 0x08, 0x8E);
    idt_set_gate(IRQ2, (uint32_t) irq2, 0x08, 0x8E);
    idt_set_gate(IRQ3, (uint32_t) irq3, 0x08, 0x8E);
    idt_set_gate(IRQ4, (uint32_t) irq4, 0x08, 0x8E);
    idt_set_gate(IRQ5, (uint32_t) irq5, 0x08, 0x8E);
    idt_set_gate(IRQ6, (uint32_t) irq6, 0x08, 0x8E);
    idt_set_gate(IRQ7, (uint32_t) irq7, 0x08, 0x8E);
    idt_set_gate(IRQ8, (uint32_t) irq8, 0x08, 0x8E);
    idt_set_gate(IRQ9, (uint32_t) irq9, 0x08, 0x8E);
    idt_set_gate(IRQ10, (uint32_t) irq10, 0x08, 0x8E);
    idt_set_gate(IRQ11, (uint32_t) irq11, 0x08, 0x8E);
    idt_set_gate(IRQ12, (uint32_t) irq12, 0x08, 0x8E);
    idt_set_gate(IRQ13, (uint32_t) irq13, 0x08, 0x8E);
    idt_set_gate(IRQ14, (uint32_t) irq14, 0x08, 0x8E);
    idt_set_gate(IRQ15, (uint32_t) irq15, 0x08, 0x8E);

    idt_flush((uint32_t) &idt_ptr);
}

/**
 * @brief Set up a GDT entry. @see gdt_entry
 */
static void
gdt_set_gate(uint32_t idx, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran) {
    gdt_entries[idx].base_low    = low_16(base);
    gdt_entries[idx].base_middle = (base >> 16) & 0xFF;
    gdt_entries[idx].base_high   = (base >> 24) & 0xFF;

    gdt_entries[idx].limit_low   = low_16(limit);
    gdt_entries[idx].granularity = (limit >> 16) & 0x0F;

    gdt_entries[idx].granularity |= gran & 0xF0;
    gdt_entries[idx].access = access;
}

/**
 * @brief Set up an IDT entry. @see idt_entry
 */
static void idt_set_gate(uint32_t idx, uint32_t base, uint16_t selector, uint8_t flags) {
    idt_entries[idx].base_lo = low_16(base);
    idt_entries[idx].base_hi = high_16(base);
    idt_entries[idx].always0 = 0; // Set to 0 just in case
    idt_entries[idx].sel     = selector;
    idt_entries[idx].flags   = flags;
}

/************************************************
 * PUBLIC FUNCTIONS                             *
 ************************************************/

void init_descriptor_tables() {
    init_gdt();
    init_idt();
}
