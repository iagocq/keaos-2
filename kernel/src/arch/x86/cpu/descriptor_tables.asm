[bits 32]
section .text

global gdt_flush
global idt_flush

gdt_flush:
    mov     eax, [esp+4] ; GDT pointer (parameter)
    lgdt    [eax]        ; Load the new GDT pointer

    mov     ax, 0x10     ; 0x10 is the offset in the GDT of our data segment
    mov     ds, ax       ; Reload all data segment registers
    mov     es, ax
    mov     fs, ax
    mov     gs, ax
    mov     ss, ax

    jmp     0x08:.flush  ; Far jump, 0x08 is the offset to our code sement
.flush:
    ret

idt_flush:
    mov     eax, [esp+4] ; IDT pointer (parameter)
    lidt    [eax]        ; Load the new IDT pointer
    ret