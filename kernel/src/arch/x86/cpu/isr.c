/**
 * @file isr.c
 * @brief High level interrupt service routines and interrupt request handlers.
 */

#include <x86/isr.h>
#include <x86/ports.h>
#include <x86/screen.h>

/* To print exception names */
static char *exception_messages[] = {
    "Division By Zero",
    "Debug",
    "Non Maskable Interrupt",
    "Breakpoint",
    "Into Detected Overflow",
    "Out of Bounds",
    "Invalid Instruction",
    "No Coprocessor",

    "Double Fault",
    "Coprocessor Segment Overrun",
    "Bad TSS",
    "Segment Not Present",
    "Stack Fault",
    "General Protection Fault",
    "Page Fault",
    "Unknown Interrupt",

    "Coprocessor Fault",
    "Alignment Check",
    "Machine Check",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",

    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
};

/************************************************
 * PRIVATE FUNCTIONS                            *
 ************************************************/

isr_t interrupt_handlers[256];

/**
 * @brief Enable hardware interrupts.
 */
void enable_interrupts() {
    asm volatile("sti");
}

/**
 * @brief Disable hardware interrupts.
 */
void disable_interrupts() {
    asm volatile("cli");
}

/**
 * @brief This function is called when an ISR interrupt occurs
 */
void isr_handler(registers_t *r) {
    kprintf("\nInterrupt 0x%02X - %s\n", r->int_no, exception_messages[r->int_no]);
    // dump_registers(r);
    kprintf("----------------------------------------\n");
    switch (r->int_no) {
        /* Disable interrupts and die when an unhandable exception occurs */
        case EXCEPT_DIVISION_BY_ZERO:
        case EXCEPT_OUT_OF_BOUNDS:
        case EXCEPT_INVALID_OPCODE:
        case EXCEPT_DEV_NOT_AVAILABLE:
        case EXCEPT_DOUBLE_FAULT:
        case EXCEPT_COPROCESSOR_SO:
        case EXCEPT_INVALID_TSS:
        case EXCEPT_SEGMENT_NOT_PRESENT:
        case EXCEPT_STACK_FAULT:
        case EXCEPT_GENERAL_PROTECTION_FAULT:
        case EXCEPT_PAGE_FAULT:
        case EXCEPT_X87_FP:
        case EXCEPT_ALIGNMENT_CHECK:
        case EXCEPT_SIMD_FP:
        case EXCEPT_VIRTUALIZATION:
        case EXCEPT_SECURITY:
            kprintf("Unrecoverable interrupt :)\n");
            disable_interrupts();
            while (1) {}
    }
}

/**
 * @brief Called when an IRQ is triggered.
 */
void irq_handler(registers_t *r) {
    /* After every interrupt we need to send an EOI to the PICs
     * or they will not send another interrupt again */
    if (r->int_no >= 40)
        outb(0xA0, 0x20); /* slave */
    outb(0x20, 0x20);     /* master */

    /* Handle the interrupt in a more modular way */
    if (interrupt_handlers[r->int_no] != 0) {
        isr_t handler = interrupt_handlers[r->int_no];
        handler(r);
    }
}

/************************************************
 * PUBLIC FUNCTIONS                            *
 ************************************************/

/**
 * @brief Register interrupt routine to an IRQ number.
 *
 * @param n IRQ number.
 * @param handler Interrupt routine
 */
void register_interrupt_handler(uint8_t n, isr_t handler) {
    interrupt_handlers[n] = handler;
}
