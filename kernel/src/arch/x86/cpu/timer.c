/**
 * @file timer.c
 * @brief Initialises the PIT, and handles clock updates.
 */

#include <x86/isr.h>
#include <x86/ports.h>
#include <x86/screen.h>
#include <x86/timer.h>

uint32_t tick = 0;

static void timer_callback(registers_t *r) {
    tick++;
}

/**
 * @brief Initialises the PIT and register IRQ callback.
 * @param freq The frequency of the PIT
 */
void init_timer(uint32_t freq) {
    /* Install the timer_callback function */
    register_interrupt_handler(IRQ0, &timer_callback);

    /* Get PIT value: hw clock 1193180 Hz */
    uint32_t divisor = 1193180 / freq;
    uint8_t  low     = (uint8_t)(divisor & 0xff);
    uint8_t  high    = (uint8_t)((divisor >> 8) & 0xff);

    /* Send the command */
    outb(0x43, 0x36); /* Command port */
    outb(0x40, low);
    outb(0x40, high);
}
