/**
 * @file main.c
 * @brief C entry point of the kernel.
 */

#include <x86/descriptor_tables.h>
#include <x86/isr.h>
#include <x86/multiboot2.h>
#include <x86/screen.h>
#include <x86/timer.h>

#include <x86/drivers/keyboard.h>
#include <x86/drivers/pci/pci.h>

#include <mem/llmalloc.h>

#include <stdint.h>

extern uint32_t _KERNEL_OFFSET;

uint8_t malloc_region[1024 * 1024];

void _start(uint32_t mb2_ptr) {
    init_screen();
    register_kputchar(&vga_kputchar_handler);

    kprintf("\n");
    kprintf("kk eae men\n");

    init_llmalloc(malloc_region, 1024 * 1024);
    // Address must be translated, since mb2 gives us the physical address
    init_mb2(mb2_ptr + (uint32_t) &_KERNEL_OFFSET);

    init_descriptor_tables();
    init_timer(5);
    init_keyboard();
    enable_interrupts();

    load_pci_devices();
    enumerate_pci();
}
