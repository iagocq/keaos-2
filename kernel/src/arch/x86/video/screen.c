/**
 * @file screen.c
 * @brief Implementation of BIOS VGA video operations.
 */

#include <ctype.h>
#include <stdarg.h>
#include <string.h>

#include <keaos/kprintf.h>
#include <string-utils.h>
#include <x86/ports.h>
#include <x86/screen.h>

static screen_t screen;
static uint8_t  kprintf_color_transform[16];

/* Private declarations */
static int  get_cursor_offset();
static void set_cursor_offset(int offset);
static int  get_offset(int col, int row);
static int  get_offset_row(int offset);
static int  get_offset_col(int offset);
static void set_vga_word(uint16_t *vga, uint16_t w);
static void print_char_at(char chr, uint8_t x, uint8_t y);
static void scroll_fb(uint8_t lines);

/************************************************
 * PRIVATE FUNCTIONS                            *
 ************************************************/

static inline void set_vga_word(uint16_t *vga, uint16_t w) {
    ((uint8_t *) vga)[0] = w & 0xff;
    ((uint8_t *) vga)[1] = w >> 8;
}

static void memset_2(void *mem, uint16_t v, size_t n) {
    uint16_t *n_mem = (uint16_t *) mem;
    for (; n > 0; n--) {
        n_mem[n] = v;
    }
}

static void print_char_at(char chr, uint8_t x, uint8_t y) {
    set_vga_word(&screen.video[y * screen.width + x],
                 ((uint8_t) chr) | (screen.current_attributes & 0xff00));
}

static void scroll_fb(uint8_t lines) {
    if (lines < screen.height) {
        memcpy(screen.video,
               &screen.video[screen.width * lines],
               screen.width * (screen.height - lines) * 2);
    }
    memset_2(&screen.video[screen.width * (screen.height - lines)] - 1,
             screen.current_attributes,
             screen.width * lines);
}

/**
 * @brief Get current cursor offset from VGA controller.
 *
 * @return int Cursor offset.
 */
static int get_cursor_offset() {
    outb(REG_SCREEN_CTRL, 14);
    int offset = inb(REG_SCREEN_DATA) << 8;
    outb(REG_SCREEN_CTRL, 15);
    offset += inb(REG_SCREEN_DATA);
    return offset * 2;
}

/**
 * @brief Set the current VGA cursor offset.
 */
static void set_cursor_offset(int offset) {
    outb(REG_SCREEN_CTRL, 14);
    outb(REG_SCREEN_DATA, (uint8_t)(offset >> 8));
    outb(REG_SCREEN_CTRL, 15);
    outb(REG_SCREEN_DATA, (uint8_t)(offset & 0xff));
}

/**
 * @brief Get cursor offset value from given column and row numbers.
 *
 * @param col Column number.
 * @param row Row number.
 * @return int Cursor offset.
 */
static inline int get_offset(int col, int row) {
    return row * MAX_COLS + col;
}

/**
 * @brief Get row number from cursor offset.
 *
 * @param offset Cursor offset.
 * @return int Row number.
 */
static inline int get_offset_row(int offset) {
    return offset / MAX_COLS;
}

/**
 * @brief Get column number from cursor offset.
 *
 * @param offset Cursor offset.
 * @return int Column number.
 */
static inline int get_offset_col(int offset) {
    return offset - (get_offset_row(offset) * MAX_COLS);
}

static inline uint16_t vga_attribute(uint8_t foreground, uint8_t background, char chr) {
    return (((VGA_COLOR_BLACK << 4) | VGA_COLOR_GRAY) << 8) | chr;
}

/************************************************
 * PUBLIC FUNCTIONS                             *
 ************************************************/

/**
 * @brief Initialize screen for usage.
 *
 */
void init_screen() {
    screen.cur_x  = 0;
    screen.cur_y  = 0;
    screen.width  = 80;
    screen.height = 25;
    // TODO let the default attributes be modifiable
    screen.current_attributes = vga_attribute(VGA_COLOR_GRAY, VGA_COLOR_BLACK, 0);
    screen.video              = (void *) VIDEO_ADDRESS;

    kprintf_color_transform[KPRINTF_COLOR_RED]     = VGA_COLOR_RED;
    kprintf_color_transform[KPRINTF_COLOR_GREEN]   = VGA_COLOR_GREEN;
    kprintf_color_transform[KPRINTF_COLOR_BLUE]    = VGA_COLOR_BLUE;
    kprintf_color_transform[KPRINTF_COLOR_CYAN]    = VGA_COLOR_CYAN;
    kprintf_color_transform[KPRINTF_COLOR_MAGENTA] = VGA_COLOR_MAGENTA;
    kprintf_color_transform[KPRINTF_COLOR_YELLOW]  = VGA_COLOR_YELLOW;
    kprintf_color_transform[KPRINTF_COLOR_WHITE]   = VGA_COLOR_WHITE;
    kprintf_color_transform[KPRINTF_COLOR_GRAY]    = VGA_COLOR_GRAY;
    kprintf_color_transform[KPRINTF_COLOR_BLACK]   = VGA_COLOR_BLACK;

    clear_screen();
}

/**
 * @brief Clear screen.
 *
 * Uses the default attributes set in init_screen()
 */
void clear_screen() {
    int       screen_size = MAX_COLS * MAX_ROWS;
    uint16_t *vga         = (uint16_t *) VIDEO_ADDRESS;

    int i;
    for (i = 0; i < screen_size; i++) {
        set_vga_word(&vga[i], screen.current_attributes);
    }

    set_cursor_offset(get_offset(0, 0));
}

/**
 * @brief Put a character on screen, advancing the cursor and scrolling the screen as needed.
 *
 * @param chr Character to print.
 */
void vga_putchar(char chr) {
    switch (chr) {
        case '\n':
            screen.cur_y += 1;

        case '\r':
            screen.cur_x = 0;
            break;

        case '\t':
            screen.cur_x += 4 - (screen.cur_x % 4);
            break;

        default:
            print_char_at(chr, screen.cur_x, screen.cur_y);
            screen.cur_x += 1;
            break;
    }
    if (screen.cur_x >= screen.width) {
        screen.cur_y += 1;
        screen.cur_x = 0;
    }
    if (screen.cur_y >= screen.height) {
        scroll_fb(1);
        screen.cur_y = screen.height - 1;
    }
    set_cursor_offset(get_offset(screen.cur_x, screen.cur_y));
}

void vga_kputchar_handler(kprintf_state_t *state) {
    uint16_t old_attribute    = screen.current_attributes;
    screen.current_attributes = vga_attribute(kprintf_color_transform[state->foreground_color],
                                              kprintf_color_transform[state->background_color],
                                              0);
    vga_putchar(state->current_char);
    screen.current_attributes = old_attribute;
}

/**
 * @brief Print various memory informations in a debug message.
 *
 */
/* void print_mem_info() {
    kprintf("Total bytes:\t\t%d\n", ll_total_bytes);
    kprintf("Free bytes:\t\t\t%d\tUsed bytes:\t\t\t%d\n",
            free_bytes(),
            ll_total_bytes - available_bytes());
    kprintf("Available bytes:\t%d\tAllocated bytes:\t%d\n",
            available_bytes(),
            ll_total_bytes - free_bytes());
} */

/**
 * @brief Print values of registers in a debug message.
 *
 * @param r Registers.
 */
/* void dump_registers(registers_t *r) {
    kprintf("eip:\t0x%08X\teflags:\t0x%08X\t\n", r->eip, r->eflags);
    kprintf(
        "eax:\t0x%08X\tebx:\t0x%08X\tecx:\t0x%08X\tedx:\t0x%08X\n", r->eax, r->ebx, r->ecx, r->edx);
    kprintf(
        "ebp:\t0x%08X\tedi:\t0x%08X\tesi:\t0x%08X\tesp:\t0x%08X\n", r->ebp, r->edi, r->esi, r->esp);
    kprintf("cs:\t0x%04X\tss:\t0x%04X\tds:\t0x%04X\t\n", r->cs, r->ss, r->ds);
    kprintf("es:\t0x%04X\tfs:\t0x%04X\tgs:\t0x%04X\t\n", r->es, r->fs, r->gs);
} */
