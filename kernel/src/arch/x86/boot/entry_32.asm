[bits 32]
section .mb2.text

%define _KERNEL_OFFSET 0xC0000000
%define _LOW_VID_START 0x000B8000
%define _LOW_VID_END   0x000BBFFF

%macro trl 2
    mov     %1, %2 - _KERNEL_OFFSET
%endmacro

global _entry
global _print_ok
extern _start
extern _kernel_start
extern _kernel_end
extern _text_start
extern _text_end
extern _rodata_start
extern _rodata_end

_entry:
    ; Please do not touch ebx
    ; Setup paging
    trl     edi, boot_page_table1
    mov     ecx, 0 ; Loop counter

page_entry:
    mov     eax, 0x1000     ; 4KiB page
    mul     ecx             ; Start address of ecx-th page
    cmp     eax, _LOW_VID_START
    jl      ro              ; Jump to ro if we are under vid mem
    cmp     eax, _LOW_VID_END
    jle     rw              ; Jump to rw if we are within vid mem
    trl     edx, _kernel_start
    cmp     eax, edx
    jl      ro              ; Jump to ro if we are under kernel
    trl     edx, _text_start
    cmp     eax, edx
    jl      rw              ; Jump to rw if we are under text
    trl     edx, _text_end
    cmp     eax, edx
    jle     ro              ; Jump to ro if we are within text
    trl     edx, _rodata_start
    cmp     eax, edx
    jl      rw              ; Jump to rw if we are under rodata
    trl     edx, _rodata_end
    cmp     eax, edx
    jle     ro              ; Jump to ro if we are within rodata
rw: or      eax, 3          ; Map frame as present + rw
    jmp     mp
ro: or      eax, 1          ; Map frame as present + ro
mp: mov     [edi + ecx * 4], eax
    inc     ecx             ; Increment the counter
    cmp     ecx, 1020       ; Compare with 1020
    jl      page_entry      ; If it's lower, jump back to page_entry
break:
    ; Map video memory to 0xC03FC000. Video memory is 16KiB, so we need
    ; 4 pages
    mov     dword [edi + 1020 * 4], 0x000B8003
    mov     dword [edi + 1021 * 4], 0x000B9003
    mov     dword [edi + 1022 * 4], 0x000BA003
    mov     dword [edi + 1023 * 4], 0x000BB003

    trl     esi, boot_page_directory
    or      edi, 3          ; Map page table 1 as present + rw
    mov     [esi], edi      ; Put page table 1 into index 0 of directory (identity map)
    mov     [esi + 768 * 4], edi ; Also put page table 1 into index 768 of directory
    mov     cr3, esi        ; Tell the processor where directory is
    mov     ecx, cr0
    or      ecx, 0x80000000 ; Set paging bit
    mov     cr0, ecx        ; Enable paging

    lea     ecx, hm         ; Effective address of high mem entry
    jmp     ecx             ; Jump to high mem


section .text
hm: mov     dword [boot_page_directory], 0 ; Clear identity mapping

    ; Flush tlb, so CPU can notice the change
    mov     ecx, cr3
    mov     cr3, ecx

    mov     esp, stack_top  ; Setup call stack
    push    ebx             ; Multiboot2 struct pointer
    call    _start
    call    _print_ok
.dead:
    hlt
    jmp .dead

_print_ok:
    mov dword [0xC03FC000], 0x2f4b2f4f
    ret

section .bss
align   4096
boot_page_directory:
    resb    4096
boot_page_table1:
    resb    4096
align 64
stack_bottom:
    resb    16384
stack_top:
