/**
 * @file multiboot2.c
 * @author vslg (slgf@protonmail.ch)
 * @brief Initializes mb2 struct passed to _start
 * @date 2020-11-01
 *
 * @copyright Copyright (c) 2020 keaOS/2 developers and contributors
 *
 * @license This project is released under GNU General Public License v3.0. Check the project root
 * for a full version of the license.
 */

#include <stddef.h>
#include <x86/multiboot2.h>
#include <x86/screen.h>

#define trace_mb2(fmt, ...) kprintf("MB2: " fmt "\n", ##__VA_ARGS__)

/************************************************
 * PRIVATE FUNCTIONS                            *
 ************************************************/

/**
 * @brief Initializes memory based on mb2 info.
 *
 * @param tag The mb2_tag_mmap pointer
 */
void mb2_init_mem(mb2_tag_mmap_t *tag) {
    mb2_mmap_entry_t *mmap = NULL;
    trace_mb2("BIOS-provided physical RAM map:");

    for (mmap = tag->entries; (uint8_t *) mmap < (uint8_t *) tag + tag->tag.size;
         mmap = (mb2_mmap_entry_t *) ((uint64_t) mmap + tag->entry_size)) {
        uint64_t end_addr = mmap->addr + mmap->len;
        kprintf("\t[mem 0x%08x%08x-0x%08x%08x] ",
                (uint32_t)(mmap->addr >> 32),
                (uint32_t) mmap->addr,
                (uint32_t)(end_addr >> 32),
                (uint32_t) end_addr - 1);
        switch (mmap->type) {
            case MB2_MEM_AVAILABLE:
                kprintf("usable   ");
                break;
            case MB2_MEM_RESERVED:
                kprintf("reserved ");
                break;
            case MB2_MEM_ACPI_RECLAIMABLE:
                kprintf("ACPI data\t");
                break;
            case MB2_MEM_NVS:
                kprintf("ACPI NVS\t");
                break;
            case MB2_MEM_BADRAM:
                kprintf("bad RAM\t");
                break;
            default:
                kprintf("\t");
                break;
        }
        kprintf("0x%04x%08x bytes\n", (uint32_t)(mmap->len >> 32), (uint32_t) mmap->len);
    }
}

/************************************************
 * PUBLIC FUNCTIONS                             *
 ************************************************/

/**
 * @brief Initializes everything mb2 gave us.
 *
 * @param mb2_ptr The pointer to mb2 info struct.
 */
void init_mb2(uint32_t mb2_ptr) {
    trace_mb2("mb2 struct address: 0x%08x", mb2_ptr);

    mb2_tag_t *tag;
    uint32_t   size;

    if (mb2_ptr & 7) {
        trace_mb2("Unaligned mb2: 0x%x", mb2_ptr);
        return;
    }

    size = *(uint32_t *) mb2_ptr;
    trace_mb2("Announced mb2 size 0x%x", size);

    for (tag = (mb2_tag_t *) (mb2_ptr + 8); tag->type != MB2_TAG_TYPE_END;
         tag = (mb2_tag_t *) ((uint8_t *) tag + ((tag->size + 7) & ~7)))
        switch (tag->type) {
            case MB2_TAG_TYPE_MMAP: {
                mb2_init_mem((mb2_tag_mmap_t *) tag);
                break;
            }
            default:
                break;
        }
}
