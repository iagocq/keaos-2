#include <string.h>

#include <mem/llmalloc.h>
#include <x86/drivers/pci/pci.h>
#include <x86/ports.h>
#include <x86/screen.h>

/* Private declarations */
static uint32_t pci_config_read_dword(uint8_t bus, uint8_t device, uint8_t func, uint8_t offset);
static void     pci_print_device(uint8_t bus, uint8_t device, uint8_t func);
static uint8_t  ll_pci_free(ll_pci_device_node_t *node);
static uint8_t  ll_pci_add_device(uint8_t bus, uint8_t slot, uint8_t func);

ll_pci_device_node_t *first_node = NULL;

/* Public declarations */
uint16_t pci_device_count = 0;

/************************************************
 * PRIVATE FUNCTIONS                            *
 ************************************************/

/**
 * @brief Read a dword for a given PCI address from the PCI configuration port.
 *
 * @param bus PCI bus.
 * @param device PCI device.
 * @param func PCI function.
 * @param offset PCI offset.
 * @return uint32_t PCI address information.
 */
static uint32_t pci_config_read_dword(uint8_t bus, uint8_t device, uint8_t func, uint8_t offset) {
    pci_config_address_t address;
    address.bus    = bus;
    address.device = device;
    address.func   = func;
    address.offset = offset;
    address.eb     = 1;

    outl(PCI_CONFIG_ADDRESS, address.data);
    return inl(PCI_CONFIG_DATA);
}

/**
 * @brief Free a PCI device from the PCI device list.
 *
 * @param node PCI device node.
 * @return uint8_t 1 if successful, 0 otherwise.
 */
static uint8_t ll_pci_free(ll_pci_device_node_t *node) {
    if (!node || !first_node)
        return 0;

    uint8_t code;

    if (node->next->next) {
        if (!(code = ll_pci_free(node->next)))
            return code;
    }

    if (!(code = llfree(node->next)))
        return code;

    node->next = NULL;
    pci_device_count--;

    if (node == first_node && !node->next) {
        if (!(code = llfree(first_node)))
            return code;

        first_node = NULL;
        pci_device_count--;
    }

    return code;
}

/**
 * @brief Add a PCI device to the devices list.
 *
 * @param bus PCI bus.
 * @param slot PCI slot.
 * @param func PCI function.
 * @return uint8_t 1 if successful, 0 otherwise.
 */
static uint8_t ll_pci_add_device(uint8_t bus, uint8_t slot, uint8_t func) {
    pci_device_t          device       = pci_get_device(bus, slot, func);
    ll_pci_device_node_t *current_node = first_node;

    if (!first_node) {
        first_node = llmalloc(sizeof(ll_pci_device_node_t));
        if (!first_node)
            return 0;
        current_node = first_node;
    } else {
        while (current_node->next != NULL)
            current_node = current_node->next;

        current_node->next = llmalloc(sizeof(ll_pci_device_node_t));
        if (!current_node->next)
            return 0;

        current_node = current_node->next;
    }

    current_node->device = device;
    current_node->next   = NULL;
    current_node->bus    = bus;
    current_node->slot   = slot;
    current_node->func   = func;

    if (device.header.header_type & 0x80) {
        for (uint8_t function = func + 1; function < 8; function++) {
            pci_device_header_t header = pci_get_device_header(bus, slot, function);
            if (header.vendor_id != 0xffff) {
                uint8_t status = ll_pci_add_device(bus, slot, function);
                if (!status)
                    return 0;
            }
        }
    }

    pci_device_count++;
    return 1;
}

/************************************************
 * PUBLIC FUNCTIONS                             *
 ************************************************/

/**
 * @brief Enumerate stored PCI devices.
 *
 * @return uint16_t Number of enumerated PCI devices.
 */
uint16_t enumerate_pci() {
    if (!first_node)
        return 0;

    uint16_t count = 0;

    ll_pci_device_node_t *current_node = first_node;
    while (current_node != NULL) {
        pci_device_header_t header = current_node->device.header;

        kprintf("%02x:%02x.%x 0x%02X (0x%02X - 0x%02X): %04x:%04x\n",
                current_node->bus,
                current_node->slot,
                current_node->func,
                header.class_code,
                header.subclass,
                header.prog_if,
                header.vendor_id,
                header.device_id);

        current_node = current_node->next;
        count++;
    }

    if (count != pci_device_count)
        kprintf("WARNING: Enumerated devices count diverges from pci_device_count\n");

    return count;
}

/**
 * @brief Get PCI device header information for a device.
 *
 * @param bus PCI bus.
 * @param device PCI device.
 * @param func PCI function.
 * @return pci_device_header_t PCI device header.
 */
pci_device_header_t pci_get_device_header(uint8_t bus, uint8_t device, uint8_t func) {
    pci_device_header_t header;
    for (uint8_t i = 0; i < 4; i++)
        header.data[i] = pci_config_read_dword(bus, device, func, i * 4);
    return header;
}

/**
 * @brief Get generic information of a PCI device.
 *
 * @param bus PCI bus.
 * @param device PCI device.
 * @param func PCI function.
 * @return pci_device_generic_info_t Miscellaneous information of the device.
 */
pci_device_generic_info_t pci_get_device_generic_info(uint8_t bus, uint8_t device, uint8_t func) {
    pci_device_generic_info_t info;
    for (uint8_t i = 4; i < 0x10; i++)
        info.data[i] = pci_config_read_dword(bus, device, func, i * 4);
    return info;
}

/**
 * @brief Get all information of a PCI device.
 *
 * @param bus PCI bus.
 * @param device PCI device.
 * @param func PCI function.
 * @return pci_device_t Information of the PCI device.
 */
pci_device_t pci_get_device(uint8_t bus, uint8_t device, uint8_t func) {
    pci_device_t dev;
    for (uint8_t i = 0; i < 0x10; i++)
        dev.data[i] = pci_config_read_dword(bus, device, func, i * 4);
    return dev;
}

/**
 * @brief Explore all PCI devices.
 *
 * @return uint16_t Number of discovered devices.
 */
uint16_t load_pci_devices() {
    if (first_node || pci_device_count != 0)
        ll_pci_free(first_node);

    pci_device_header_t current;

    for (uint8_t bus = 0; bus < 0xff; bus++)
        for (uint8_t slot = 0; slot < 0x20; slot++) {
            current = pci_get_device_header(bus, slot, 0);

            if (current.vendor_id == 0xffff)
                continue;

            uint8_t status = ll_pci_add_device(bus, slot, 0);
            if (!status)
                return pci_device_count;
        }

    return pci_device_count;
}

/**
 * @brief Free all explored PCI devices.
 *
 * @return uint8_t 1 if successful, 0 otherwise.
 */
uint8_t unload_pci_devices() {
    return ll_pci_free(first_node);
}
