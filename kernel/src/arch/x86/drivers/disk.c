#include <drivers/disk.h>
#include <low_mem.h>
#include <mem.h>
#include <types.h>

/**
 * @brief Cache important disk parameters (drive number, bytes per sector) for later usage.
 *
 */
void init_disk(uint8_t drive_number) {
    disk.drive_number = drive_number;

    struct {
        uint16_t size;
        uint16_t flags;
        uint32_t cylinders, heads, spt;
        uint32_t sectors_lo, sectors_hi;
        uint16_t bps;
        uint32_t edd_p;
    } __attribute__((packed)) *res_buffer = low_mem_end;

    res_buffer->size = 0x1e;

    gp_registers_t registers = {0};
    registers.ah             = 0x48;
    registers.dl             = drive_number;
    registers.esi            = (uint32_t) res_buffer;

    real_int(0x13, registers);

    disk.bytes_per_sector = res_buffer->bps;

    int sectors = read_sectors(1, &disk.bootsector.mbr_bs, 0, 0);
    if (sectors == 0) {
        while (1) {}
    }
}

/**
 * @brief Read sectors from saved drive number.
 *
 * The number of bytes to read at a time shouldn't exceed 61000, about
 * (0xffff-0x1000). `memory` should preferentially be above 0x7ffff because of
 * how low_mem(0) works internally.
 *
 * @param sectors Number of sectors to read.
 * @param memory Pointer to where the data read should be written.
 * @param start_sector_low Lower 32 bits of the desired sector number to be read.
 * @param start_sector_high High 32 bits of the desired sector number to be read.
 *
 * @return int Always 1.
 */
int read_sectors(uint16_t sectors,
                 void *   memory,
                 uint32_t start_sector_low,
                 uint32_t start_sector_high) {
    struct {
        uint8_t  size;
        uint8_t  zero;
        uint16_t readn;
        uint32_t buf;
        uint32_t start_lo;
        uint32_t start_hi;
    } __attribute__((packed)) *dap = low_mem_end;

    dap->size     = 0x10;
    dap->zero     = 0;
    dap->readn    = sectors;
    dap->buf      = 0xa000;
    dap->start_lo = start_sector_low;
    dap->start_hi = start_sector_high;

    gp_registers_t registers = {0};
    registers.ah             = 0x42;
    registers.dl             = disk.drive_number;
    registers.esi            = (uint32_t) dap;

    real_int(0x13, registers);

    memcpy((void *) dap->buf, memory, sectors * disk.bytes_per_sector);
    return 1;
}
