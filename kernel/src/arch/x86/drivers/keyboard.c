#include <x86/drivers/keyboard.h>
#include <x86/isr.h>
#include <x86/ports.h>
#include <x86/screen.h>

key_state cs1_key_lookup_single[0xFF];
key_state cs1_key_lookup_E0[0xFF];

cs1_read_state current_cs1_state = CS1_WAITING;
//
// cs1_key
//
// void cs1_handle_scan(uint8_t scan) {
//     cs1_key key;
//
//     key = match_cs1_scan(current_cs1_state, scan);
// }

static void keyboard_callback(registers_t *r) {
    /* The key code is available in port 0x60 */
    uint8_t scan = inb(0x60);

    cs1_read_state initial_state = current_cs1_state;

    key_state key;

    if (scan == 0xE0) {
        if (current_cs1_state == CS1_WAITING) {
            current_cs1_state = CS1_READ_E0;
        } else if (current_cs1_state == CS1_READ_E0_2A) {
            current_cs1_state = CS1_READ_E0_2A_E0;
        } else {
            current_cs1_state = CS1_READ_E0_B7_E0;
        }
    } else if (scan == 0xE1) {
        if (current_cs1_state == CS1_READ_E1_1D_45) {
            current_cs1_state = CS1_READ_E1_1D_45_E1;
        } else {
            current_cs1_state = CS1_READ_E1;
        }
    } else if (scan == 0x1D && current_cs1_state == CS1_READ_E1) {
        current_cs1_state = CS1_READ_E1_1D;
    } else if (scan == 0x45 && current_cs1_state == CS1_READ_E1_1D) {
        current_cs1_state = CS1_READ_E1_1D_45;
    } else if (scan == 0x9D && current_cs1_state == CS1_READ_E1_1D_45_E1) {
        current_cs1_state = CS1_READ_E1_1D_45_E1_9D;
    } else {
        if (current_cs1_state == CS1_READ_E0) {
            if (scan == 0x2A) {
                current_cs1_state = CS1_READ_E0_2A;
            } else if (scan == 0xB7) {
                current_cs1_state = CS1_READ_E0_B7;
            } else {
                key = cs1_key_lookup_E0[scan];
            }
        } else if (current_cs1_state == CS1_READ_E0_2A_E0) {
            key = (key_state){KEYCODE_PRINT_SCREEN, 0};
        } else if (current_cs1_state == CS1_READ_E0_B7_E0) {
            key = (key_state){KEYCODE_PRINT_SCREEN, 1};
        } else if (current_cs1_state == CS1_READ_E1_1D_45_E1_9D) {
            key = (key_state){KEYCODE_PAUSE, 2};
        } else {
            key = cs1_key_lookup_single[scan];
        }
    }

    if (initial_state != current_cs1_state) {
        return;
    }

    current_cs1_state = CS1_WAITING;

    kprintf("Key %s!\tkeycode = %02x, scan = %02x\n",
            key.state == 1 ? "released" : "pressed",
            key.key,
            scan);
}

/**
 * @brief Initialize keyboard for key handling with interrupts.
 *
 */
void init_keyboard() {
    /* DONT DEAD   *
     * OPEN INSIDE */
    cs1_key_lookup_single[CS1_D_ESC]           = (key_state){KEYCODE_ESC, 0};
    cs1_key_lookup_single[CS1_D_1]             = (key_state){KEYCODE_1, 0};
    cs1_key_lookup_single[CS1_D_2]             = (key_state){KEYCODE_2, 0};
    cs1_key_lookup_single[CS1_D_3]             = (key_state){KEYCODE_3, 0};
    cs1_key_lookup_single[CS1_D_4]             = (key_state){KEYCODE_4, 0};
    cs1_key_lookup_single[CS1_D_5]             = (key_state){KEYCODE_5, 0};
    cs1_key_lookup_single[CS1_D_6]             = (key_state){KEYCODE_6, 0};
    cs1_key_lookup_single[CS1_D_7]             = (key_state){KEYCODE_7, 0};
    cs1_key_lookup_single[CS1_D_8]             = (key_state){KEYCODE_8, 0};
    cs1_key_lookup_single[CS1_D_9]             = (key_state){KEYCODE_9, 0};
    cs1_key_lookup_single[CS1_D_0]             = (key_state){KEYCODE_0, 0};
    cs1_key_lookup_single[CS1_D_DASH]          = (key_state){KEYCODE_DASH, 0};
    cs1_key_lookup_single[CS1_D_EQUALS]        = (key_state){KEYCODE_EQUALS, 0};
    cs1_key_lookup_single[CS1_D_BACKSPACE]     = (key_state){KEYCODE_BACKSPACE, 0};
    cs1_key_lookup_single[CS1_D_TAB]           = (key_state){KEYCODE_TAB, 0};
    cs1_key_lookup_single[CS1_D_Q]             = (key_state){KEYCODE_Q, 0};
    cs1_key_lookup_single[CS1_D_W]             = (key_state){KEYCODE_W, 0};
    cs1_key_lookup_single[CS1_D_E]             = (key_state){KEYCODE_E, 0};
    cs1_key_lookup_single[CS1_D_R]             = (key_state){KEYCODE_R, 0};
    cs1_key_lookup_single[CS1_D_T]             = (key_state){KEYCODE_T, 0};
    cs1_key_lookup_single[CS1_D_Y]             = (key_state){KEYCODE_Y, 0};
    cs1_key_lookup_single[CS1_D_U]             = (key_state){KEYCODE_U, 0};
    cs1_key_lookup_single[CS1_D_I]             = (key_state){KEYCODE_I, 0};
    cs1_key_lookup_single[CS1_D_O]             = (key_state){KEYCODE_O, 0};
    cs1_key_lookup_single[CS1_D_P]             = (key_state){KEYCODE_P, 0};
    cs1_key_lookup_single[CS1_D_OPEN_SQBRACE]  = (key_state){KEYCODE_OPEN_SQBRACE, 0};
    cs1_key_lookup_single[CS1_D_CLOSE_SQBRACE] = (key_state){KEYCODE_CLOSE_SQBRACE, 0};
    cs1_key_lookup_single[CS1_D_ENTER]         = (key_state){KEYCODE_ENTER, 0};
    cs1_key_lookup_single[CS1_D_LEFT_CTRL]     = (key_state){KEYCODE_LEFT_CTRL, 0};
    cs1_key_lookup_single[CS1_D_A]             = (key_state){KEYCODE_A, 0};
    cs1_key_lookup_single[CS1_D_S]             = (key_state){KEYCODE_S, 0};
    cs1_key_lookup_single[CS1_D_D]             = (key_state){KEYCODE_D, 0};
    cs1_key_lookup_single[CS1_D_F]             = (key_state){KEYCODE_F, 0};
    cs1_key_lookup_single[CS1_D_G]             = (key_state){KEYCODE_G, 0};
    cs1_key_lookup_single[CS1_D_H]             = (key_state){KEYCODE_H, 0};
    cs1_key_lookup_single[CS1_D_J]             = (key_state){KEYCODE_J, 0};
    cs1_key_lookup_single[CS1_D_K]             = (key_state){KEYCODE_K, 0};
    cs1_key_lookup_single[CS1_D_L]             = (key_state){KEYCODE_L, 0};
    cs1_key_lookup_single[CS1_D_SEMICOLON]     = (key_state){KEYCODE_SEMICOLON, 0};
    cs1_key_lookup_single[CS1_D_QUOTE]         = (key_state){KEYCODE_QUOTE, 0};
    cs1_key_lookup_single[CS1_D_BACKTICK]      = (key_state){KEYCODE_BACKTICK, 0};
    cs1_key_lookup_single[CS1_D_LEFT_SHIFT]    = (key_state){KEYCODE_LEFT_SHIFT, 0};
    cs1_key_lookup_single[CS1_D_BACKSLASH]     = (key_state){KEYCODE_BACKSLASH, 0};
    cs1_key_lookup_single[CS1_D_Z]             = (key_state){KEYCODE_Z, 0};
    cs1_key_lookup_single[CS1_D_X]             = (key_state){KEYCODE_X, 0};
    cs1_key_lookup_single[CS1_D_C]             = (key_state){KEYCODE_C, 0};
    cs1_key_lookup_single[CS1_D_V]             = (key_state){KEYCODE_V, 0};
    cs1_key_lookup_single[CS1_D_B]             = (key_state){KEYCODE_B, 0};
    cs1_key_lookup_single[CS1_D_N]             = (key_state){KEYCODE_N, 0};
    cs1_key_lookup_single[CS1_D_M]             = (key_state){KEYCODE_M, 0};
    cs1_key_lookup_single[CS1_D_COMMA]         = (key_state){KEYCODE_COMMA, 0};
    cs1_key_lookup_single[CS1_D_PERIOD]        = (key_state){KEYCODE_PERIOD, 0};
    cs1_key_lookup_single[CS1_D_SLASH]         = (key_state){KEYCODE_SLASH, 0};
    cs1_key_lookup_single[CS1_D_RIGHT_SHIFT]   = (key_state){KEYCODE_RIGHT_SHIFT, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_STAR]   = (key_state){KEYCODE_KEYPAD_STAR, 0};
    cs1_key_lookup_single[CS1_D_LEFT_ALT]      = (key_state){KEYCODE_LEFT_ALT, 0};
    cs1_key_lookup_single[CS1_D_SPACE]         = (key_state){KEYCODE_SPACE, 0};
    cs1_key_lookup_single[CS1_D_CAPS_LOCK]     = (key_state){KEYCODE_CAPS_LOCK, 0};
    cs1_key_lookup_single[CS1_D_F1]            = (key_state){KEYCODE_F1, 0};
    cs1_key_lookup_single[CS1_D_F2]            = (key_state){KEYCODE_F2, 0};
    cs1_key_lookup_single[CS1_D_F3]            = (key_state){KEYCODE_F3, 0};
    cs1_key_lookup_single[CS1_D_F4]            = (key_state){KEYCODE_F4, 0};
    cs1_key_lookup_single[CS1_D_F5]            = (key_state){KEYCODE_F5, 0};
    cs1_key_lookup_single[CS1_D_F6]            = (key_state){KEYCODE_F6, 0};
    cs1_key_lookup_single[CS1_D_F7]            = (key_state){KEYCODE_F7, 0};
    cs1_key_lookup_single[CS1_D_F8]            = (key_state){KEYCODE_F8, 0};
    cs1_key_lookup_single[CS1_D_F9]            = (key_state){KEYCODE_F9, 0};
    cs1_key_lookup_single[CS1_D_F10]           = (key_state){KEYCODE_F10, 0};
    cs1_key_lookup_single[CS1_D_NUMBER_LOCK]   = (key_state){KEYCODE_NUMBER_LOCK, 0};
    cs1_key_lookup_single[CS1_D_SCROLL_LOCK]   = (key_state){KEYCODE_SCROLL_LOCK, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_7]      = (key_state){KEYCODE_KEYPAD_7, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_8]      = (key_state){KEYCODE_KEYPAD_8, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_9]      = (key_state){KEYCODE_KEYPAD_9, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_MINUS]  = (key_state){KEYCODE_KEYPAD_MINUS, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_4]      = (key_state){KEYCODE_KEYPAD_4, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_5]      = (key_state){KEYCODE_KEYPAD_5, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_6]      = (key_state){KEYCODE_KEYPAD_6, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_PLUS]   = (key_state){KEYCODE_KEYPAD_PLUS, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_1]      = (key_state){KEYCODE_KEYPAD_1, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_2]      = (key_state){KEYCODE_KEYPAD_2, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_3]      = (key_state){KEYCODE_KEYPAD_3, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_0]      = (key_state){KEYCODE_KEYPAD_0, 0};
    cs1_key_lookup_single[CS1_D_KEYPAD_DOT]    = (key_state){KEYCODE_KEYPAD_DOT, 0};
    cs1_key_lookup_single[CS1_D_102]           = (key_state){KEYCODE_102, 0};
    cs1_key_lookup_single[CS1_D_F11]           = (key_state){KEYCODE_F11, 0};
    cs1_key_lookup_single[CS1_D_F12]           = (key_state){KEYCODE_F12, 0};
    cs1_key_lookup_single[CS1_D_115]           = (key_state){KEYCODE_115, 0};

    cs1_key_lookup_single[CS1_U_ESC]           = (key_state){KEYCODE_ESC, 1};
    cs1_key_lookup_single[CS1_U_1]             = (key_state){KEYCODE_1, 1};
    cs1_key_lookup_single[CS1_U_2]             = (key_state){KEYCODE_2, 1};
    cs1_key_lookup_single[CS1_U_3]             = (key_state){KEYCODE_3, 1};
    cs1_key_lookup_single[CS1_U_4]             = (key_state){KEYCODE_4, 1};
    cs1_key_lookup_single[CS1_U_5]             = (key_state){KEYCODE_5, 1};
    cs1_key_lookup_single[CS1_U_6]             = (key_state){KEYCODE_6, 1};
    cs1_key_lookup_single[CS1_U_7]             = (key_state){KEYCODE_7, 1};
    cs1_key_lookup_single[CS1_U_8]             = (key_state){KEYCODE_8, 1};
    cs1_key_lookup_single[CS1_U_9]             = (key_state){KEYCODE_9, 1};
    cs1_key_lookup_single[CS1_U_0]             = (key_state){KEYCODE_0, 1};
    cs1_key_lookup_single[CS1_U_DASH]          = (key_state){KEYCODE_DASH, 1};
    cs1_key_lookup_single[CS1_U_EQUALS]        = (key_state){KEYCODE_EQUALS, 1};
    cs1_key_lookup_single[CS1_U_BACKSPACE]     = (key_state){KEYCODE_BACKSPACE, 1};
    cs1_key_lookup_single[CS1_U_TAB]           = (key_state){KEYCODE_TAB, 1};
    cs1_key_lookup_single[CS1_U_Q]             = (key_state){KEYCODE_Q, 1};
    cs1_key_lookup_single[CS1_U_W]             = (key_state){KEYCODE_W, 1};
    cs1_key_lookup_single[CS1_U_E]             = (key_state){KEYCODE_E, 1};
    cs1_key_lookup_single[CS1_U_R]             = (key_state){KEYCODE_R, 1};
    cs1_key_lookup_single[CS1_U_T]             = (key_state){KEYCODE_T, 1};
    cs1_key_lookup_single[CS1_U_Y]             = (key_state){KEYCODE_Y, 1};
    cs1_key_lookup_single[CS1_U_U]             = (key_state){KEYCODE_U, 1};
    cs1_key_lookup_single[CS1_U_I]             = (key_state){KEYCODE_I, 1};
    cs1_key_lookup_single[CS1_U_O]             = (key_state){KEYCODE_O, 1};
    cs1_key_lookup_single[CS1_U_P]             = (key_state){KEYCODE_P, 1};
    cs1_key_lookup_single[CS1_U_OPEN_SQBRACE]  = (key_state){KEYCODE_OPEN_SQBRACE, 1};
    cs1_key_lookup_single[CS1_U_CLOSE_SQBRACE] = (key_state){KEYCODE_CLOSE_SQBRACE, 1};
    cs1_key_lookup_single[CS1_U_ENTER]         = (key_state){KEYCODE_ENTER, 1};
    cs1_key_lookup_single[CS1_U_LEFT_CTRL]     = (key_state){KEYCODE_LEFT_CTRL, 1};
    cs1_key_lookup_single[CS1_U_A]             = (key_state){KEYCODE_A, 1};
    cs1_key_lookup_single[CS1_U_S]             = (key_state){KEYCODE_S, 1};
    cs1_key_lookup_single[CS1_U_D]             = (key_state){KEYCODE_D, 1};
    cs1_key_lookup_single[CS1_U_F]             = (key_state){KEYCODE_F, 1};
    cs1_key_lookup_single[CS1_U_G]             = (key_state){KEYCODE_G, 1};
    cs1_key_lookup_single[CS1_U_H]             = (key_state){KEYCODE_H, 1};
    cs1_key_lookup_single[CS1_U_J]             = (key_state){KEYCODE_J, 1};
    cs1_key_lookup_single[CS1_U_K]             = (key_state){KEYCODE_K, 1};
    cs1_key_lookup_single[CS1_U_L]             = (key_state){KEYCODE_L, 1};
    cs1_key_lookup_single[CS1_U_SEMICOLON]     = (key_state){KEYCODE_SEMICOLON, 1};
    cs1_key_lookup_single[CS1_U_QUOTE]         = (key_state){KEYCODE_QUOTE, 1};
    cs1_key_lookup_single[CS1_U_BACKTICK]      = (key_state){KEYCODE_BACKTICK, 1};
    cs1_key_lookup_single[CS1_U_LEFT_SHIFT]    = (key_state){KEYCODE_LEFT_SHIFT, 1};
    cs1_key_lookup_single[CS1_U_BACKSLASH]     = (key_state){KEYCODE_BACKSLASH, 1};
    cs1_key_lookup_single[CS1_U_Z]             = (key_state){KEYCODE_Z, 1};
    cs1_key_lookup_single[CS1_U_X]             = (key_state){KEYCODE_X, 1};
    cs1_key_lookup_single[CS1_U_C]             = (key_state){KEYCODE_C, 1};
    cs1_key_lookup_single[CS1_U_V]             = (key_state){KEYCODE_V, 1};
    cs1_key_lookup_single[CS1_U_B]             = (key_state){KEYCODE_B, 1};
    cs1_key_lookup_single[CS1_U_N]             = (key_state){KEYCODE_N, 1};
    cs1_key_lookup_single[CS1_U_M]             = (key_state){KEYCODE_M, 1};
    cs1_key_lookup_single[CS1_U_COMMA]         = (key_state){KEYCODE_COMMA, 1};
    cs1_key_lookup_single[CS1_U_PERIOD]        = (key_state){KEYCODE_PERIOD, 1};
    cs1_key_lookup_single[CS1_U_SLASH]         = (key_state){KEYCODE_SLASH, 1};
    cs1_key_lookup_single[CS1_U_RIGHT_SHIFT]   = (key_state){KEYCODE_RIGHT_SHIFT, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_STAR]   = (key_state){KEYCODE_KEYPAD_STAR, 1};
    cs1_key_lookup_single[CS1_U_LEFT_ALT]      = (key_state){KEYCODE_LEFT_ALT, 1};
    cs1_key_lookup_single[CS1_U_SPACE]         = (key_state){KEYCODE_SPACE, 1};
    cs1_key_lookup_single[CS1_U_CAPS_LOCK]     = (key_state){KEYCODE_CAPS_LOCK, 1};
    cs1_key_lookup_single[CS1_U_F1]            = (key_state){KEYCODE_F1, 1};
    cs1_key_lookup_single[CS1_U_F2]            = (key_state){KEYCODE_F2, 1};
    cs1_key_lookup_single[CS1_U_F3]            = (key_state){KEYCODE_F3, 1};
    cs1_key_lookup_single[CS1_U_F4]            = (key_state){KEYCODE_F4, 1};
    cs1_key_lookup_single[CS1_U_F5]            = (key_state){KEYCODE_F5, 1};
    cs1_key_lookup_single[CS1_U_F6]            = (key_state){KEYCODE_F6, 1};
    cs1_key_lookup_single[CS1_U_F7]            = (key_state){KEYCODE_F7, 1};
    cs1_key_lookup_single[CS1_U_F8]            = (key_state){KEYCODE_F8, 1};
    cs1_key_lookup_single[CS1_U_F9]            = (key_state){KEYCODE_F9, 1};
    cs1_key_lookup_single[CS1_U_F10]           = (key_state){KEYCODE_F10, 1};
    cs1_key_lookup_single[CS1_U_NUMBER_LOCK]   = (key_state){KEYCODE_NUMBER_LOCK, 1};
    cs1_key_lookup_single[CS1_U_SCROLL_LOCK]   = (key_state){KEYCODE_SCROLL_LOCK, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_7]      = (key_state){KEYCODE_KEYPAD_7, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_8]      = (key_state){KEYCODE_KEYPAD_8, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_9]      = (key_state){KEYCODE_KEYPAD_9, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_MINUS]  = (key_state){KEYCODE_KEYPAD_MINUS, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_4]      = (key_state){KEYCODE_KEYPAD_4, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_5]      = (key_state){KEYCODE_KEYPAD_5, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_6]      = (key_state){KEYCODE_KEYPAD_6, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_PLUS]   = (key_state){KEYCODE_KEYPAD_PLUS, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_1]      = (key_state){KEYCODE_KEYPAD_1, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_2]      = (key_state){KEYCODE_KEYPAD_2, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_3]      = (key_state){KEYCODE_KEYPAD_3, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_0]      = (key_state){KEYCODE_KEYPAD_0, 1};
    cs1_key_lookup_single[CS1_U_KEYPAD_DOT]    = (key_state){KEYCODE_KEYPAD_DOT, 1};
    cs1_key_lookup_single[CS1_U_102]           = (key_state){KEYCODE_102, 1};
    cs1_key_lookup_single[CS1_U_F11]           = (key_state){KEYCODE_F11, 1};
    cs1_key_lookup_single[CS1_U_F12]           = (key_state){KEYCODE_F12, 1};
    cs1_key_lookup_single[CS1_U_115]           = (key_state){KEYCODE_115, 1};

    cs1_key_lookup_E0[CS1_D_MM_PREV_TRACK]    = (key_state){KEYCODE_MM_PREV_TRACK, 0};
    cs1_key_lookup_E0[CS1_D_MM_NEXT_TRACK]    = (key_state){KEYCODE_MM_NEXT_TRACK, 0};
    cs1_key_lookup_E0[CS1_D_KEYPAD_ENTER]     = (key_state){KEYCODE_KEYPAD_ENTER, 0};
    cs1_key_lookup_E0[CS1_D_RIGHT_CONTROL]    = (key_state){KEYCODE_RIGHT_CONTROL, 0};
    cs1_key_lookup_E0[CS1_D_MM_MUTE]          = (key_state){KEYCODE_MM_MUTE, 0};
    cs1_key_lookup_E0[CS1_D_MM_STOP]          = (key_state){KEYCODE_MM_STOP, 0};
    cs1_key_lookup_E0[CS1_D_MM_VOL_DOWN]      = (key_state){KEYCODE_MM_VOL_DOWN, 0};
    cs1_key_lookup_E0[CS1_D_MM_VOL_UP]        = (key_state){KEYCODE_MM_VOL_UP, 0};
    cs1_key_lookup_E0[CS1_D_MM_WWW_HOME]      = (key_state){KEYCODE_MM_WWW_HOME, 0};
    cs1_key_lookup_E0[CS1_D_KEYPAD_DIVIDE]    = (key_state){KEYCODE_KEYPAD_DIVIDE, 0};
    cs1_key_lookup_E0[CS1_D_RIGHT_ALT]        = (key_state){KEYCODE_RIGHT_ALT, 0};
    cs1_key_lookup_E0[CS1_D_HOME]             = (key_state){KEYCODE_HOME, 0};
    cs1_key_lookup_E0[CS1_D_CURSOR_UP]        = (key_state){KEYCODE_CURSOR_UP, 0};
    cs1_key_lookup_E0[CS1_D_PAGE_UP]          = (key_state){KEYCODE_PAGE_UP, 0};
    cs1_key_lookup_E0[CS1_D_CURSOR_LEFT]      = (key_state){KEYCODE_CURSOR_LEFT, 0};
    cs1_key_lookup_E0[CS1_D_CURSOR_RIGHT]     = (key_state){KEYCODE_CURSOR_RIGHT, 0};
    cs1_key_lookup_E0[CS1_D_END]              = (key_state){KEYCODE_END, 0};
    cs1_key_lookup_E0[CS1_D_CURSOR_DOWN]      = (key_state){KEYCODE_CURSOR_DOWN, 0};
    cs1_key_lookup_E0[CS1_D_PAGE_DOWN]        = (key_state){KEYCODE_PAGE_DOWN, 0};
    cs1_key_lookup_E0[CS1_D_INSERT]           = (key_state){KEYCODE_INSERT, 0};
    cs1_key_lookup_E0[CS1_D_DELETE]           = (key_state){KEYCODE_DELETE, 0};
    cs1_key_lookup_E0[CS1_D_LEFT_GUI]         = (key_state){KEYCODE_LEFT_GUI, 0};
    cs1_key_lookup_E0[CS1_D_RIGHT_GUI]        = (key_state){KEYCODE_RIGHT_GUI, 0};
    cs1_key_lookup_E0[CS1_D_APPS]             = (key_state){KEYCODE_APPS, 0};
    cs1_key_lookup_E0[CS1_D_ACPI_POWER]       = (key_state){KEYCODE_ACPI_POWER, 0};
    cs1_key_lookup_E0[CS1_D_ACPI_SLEEP]       = (key_state){KEYCODE_ACPI_SLEEP, 0};
    cs1_key_lookup_E0[CS1_D_ACPI_WAKE]        = (key_state){KEYCODE_ACPI_WAKE, 0};
    cs1_key_lookup_E0[CS1_D_MM_WWW_SEARCH]    = (key_state){KEYCODE_MM_WWW_SEARCH, 0};
    cs1_key_lookup_E0[CS1_D_MM_WWW_FAVORITES] = (key_state){KEYCODE_MM_WWW_FAVORITES, 0};
    cs1_key_lookup_E0[CS1_D_MM_WWW_REFRESH]   = (key_state){KEYCODE_MM_WWW_REFRESH, 0};
    cs1_key_lookup_E0[CS1_D_MM_WWW_STOP]      = (key_state){KEYCODE_MM_WWW_STOP, 0};
    cs1_key_lookup_E0[CS1_D_MM_WWW_FORWARD]   = (key_state){KEYCODE_MM_WWW_FORWARD, 0};
    cs1_key_lookup_E0[CS1_D_MM_WWW_BACK]      = (key_state){KEYCODE_MM_WWW_BACK, 0};
    cs1_key_lookup_E0[CS1_D_MM_MY_COMPUTER]   = (key_state){KEYCODE_MM_MY_COMPUTER, 0};
    cs1_key_lookup_E0[CS1_D_MM_EMAIL]         = (key_state){KEYCODE_MM_EMAIL, 0};
    cs1_key_lookup_E0[CS1_D_MM_MEDIA_SELECT]  = (key_state){KEYCODE_MM_MEDIA_SELECT, 0};

    cs1_key_lookup_E0[CS1_U_MM_PREV_TRACK]    = (key_state){KEYCODE_MM_PREV_TRACK, 1};
    cs1_key_lookup_E0[CS1_U_MM_NEXT_TRACK]    = (key_state){KEYCODE_MM_NEXT_TRACK, 1};
    cs1_key_lookup_E0[CS1_U_KEYPAD_ENTER]     = (key_state){KEYCODE_KEYPAD_ENTER, 1};
    cs1_key_lookup_E0[CS1_U_RIGHT_CONTROL]    = (key_state){KEYCODE_RIGHT_CONTROL, 1};
    cs1_key_lookup_E0[CS1_U_MM_MUTE]          = (key_state){KEYCODE_MM_MUTE, 1};
    cs1_key_lookup_E0[CS1_U_MM_STOP]          = (key_state){KEYCODE_MM_STOP, 1};
    cs1_key_lookup_E0[CS1_U_MM_VOL_DOWN]      = (key_state){KEYCODE_MM_VOL_DOWN, 1};
    cs1_key_lookup_E0[CS1_U_MM_VOL_UP]        = (key_state){KEYCODE_MM_VOL_UP, 1};
    cs1_key_lookup_E0[CS1_U_MM_WWW_HOME]      = (key_state){KEYCODE_MM_WWW_HOME, 1};
    cs1_key_lookup_E0[CS1_U_KEYPAD_DIVIDE]    = (key_state){KEYCODE_KEYPAD_DIVIDE, 1};
    cs1_key_lookup_E0[CS1_U_RIGHT_ALT]        = (key_state){KEYCODE_RIGHT_ALT, 1};
    cs1_key_lookup_E0[CS1_U_HOME]             = (key_state){KEYCODE_HOME, 1};
    cs1_key_lookup_E0[CS1_U_CURSOR_UP]        = (key_state){KEYCODE_CURSOR_UP, 1};
    cs1_key_lookup_E0[CS1_U_PAGE_UP]          = (key_state){KEYCODE_PAGE_UP, 1};
    cs1_key_lookup_E0[CS1_U_CURSOR_LEFT]      = (key_state){KEYCODE_CURSOR_LEFT, 1};
    cs1_key_lookup_E0[CS1_U_CURSOR_RIGHT]     = (key_state){KEYCODE_CURSOR_RIGHT, 1};
    cs1_key_lookup_E0[CS1_U_END]              = (key_state){KEYCODE_END, 1};
    cs1_key_lookup_E0[CS1_U_CURSOR_DOWN]      = (key_state){KEYCODE_CURSOR_DOWN, 1};
    cs1_key_lookup_E0[CS1_U_PAGE_DOWN]        = (key_state){KEYCODE_PAGE_DOWN, 1};
    cs1_key_lookup_E0[CS1_U_INSERT]           = (key_state){KEYCODE_INSERT, 1};
    cs1_key_lookup_E0[CS1_U_DELETE]           = (key_state){KEYCODE_DELETE, 1};
    cs1_key_lookup_E0[CS1_U_LEFT_GUI]         = (key_state){KEYCODE_LEFT_GUI, 1};
    cs1_key_lookup_E0[CS1_U_RIGHT_GUI]        = (key_state){KEYCODE_RIGHT_GUI, 1};
    cs1_key_lookup_E0[CS1_U_APPS]             = (key_state){KEYCODE_APPS, 1};
    cs1_key_lookup_E0[CS1_U_ACPI_POWER]       = (key_state){KEYCODE_ACPI_POWER, 1};
    cs1_key_lookup_E0[CS1_U_ACPI_SLEEP]       = (key_state){KEYCODE_ACPI_SLEEP, 1};
    cs1_key_lookup_E0[CS1_U_ACPI_WAKE]        = (key_state){KEYCODE_ACPI_WAKE, 1};
    cs1_key_lookup_E0[CS1_U_MM_WWW_SEARCH]    = (key_state){KEYCODE_MM_WWW_SEARCH, 1};
    cs1_key_lookup_E0[CS1_U_MM_WWW_FAVORITES] = (key_state){KEYCODE_MM_WWW_FAVORITES, 1};
    cs1_key_lookup_E0[CS1_U_MM_WWW_REFRESH]   = (key_state){KEYCODE_MM_WWW_REFRESH, 1};
    cs1_key_lookup_E0[CS1_U_MM_WWW_STOP]      = (key_state){KEYCODE_MM_WWW_STOP, 1};
    cs1_key_lookup_E0[CS1_U_MM_WWW_FORWARD]   = (key_state){KEYCODE_MM_WWW_FORWARD, 1};
    cs1_key_lookup_E0[CS1_U_MM_WWW_BACK]      = (key_state){KEYCODE_MM_WWW_BACK, 1};
    cs1_key_lookup_E0[CS1_U_MM_MY_COMPUTER]   = (key_state){KEYCODE_MM_MY_COMPUTER, 1};
    cs1_key_lookup_E0[CS1_U_MM_EMAIL]         = (key_state){KEYCODE_MM_EMAIL, 1};
    cs1_key_lookup_E0[CS1_U_MM_MEDIA_SELECT]  = (key_state){KEYCODE_MM_MEDIA_SELECT, 1};

    register_interrupt_handler(IRQ1, &keyboard_callback);
}
