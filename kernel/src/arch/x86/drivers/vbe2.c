#include <drivers/vbe2.h>
#include <low_mem.h>
#include <mem.h>
#include <string.h>

/**
 * @brief Get VBE2 info.
 *
 * Uses real mode interrupts.
 *
 * @param vbe_info Pointer to a @ref vbe2_info_t struct to hold VBE info.
 * @return int 1 if operation was successful, 0 otherwise.
 */
int vbe2_get_bios_info(vbe2_info_t *vbe_info) {
    gp_registers_t registers = {0};
    registers.ax             = 0x4f00;
    registers.di             = (uint16_t)((uint32_t) vbe_info);
    memcpy(vbe_info->signature, "VBE2", 4);

    int_registers_t res = real_int(0x10, registers);

    return res.gp_registers.ax == 0x004f && strncmp("VESA", (char *) vbe_info->signature, 4) == 0;
}

/**
 * @brief Get info of a specific VBE2 mode.
 *
 * @param mode Mode number.
 * @param vbe_mode_info Pointer to a @ref vbe2_info_t struct to hold VBE mode info.
 * @return int 1 if operation was successful, 0 otherwise.
 */
int vbe2_get_mode_info(uint16_t mode, vbe2_mode_info_t *vbe_mode_info) {
    gp_registers_t registers = {0};
    registers.ax             = 0x4f01;
    registers.cx             = mode;
    registers.di             = (uint16_t)((uint32_t) vbe_mode_info);

    int_registers_t res = real_int(0x10, registers);

    return res.gp_registers.ax == 0x004f;
}

int        abs_diff(int a, int b);
inline int abs_diff(int a, int b) {
    if (a > b) {
        return a - b;
    } else {
        return b - a;
    }
}

/**
 * @brief Get the best-matching VBE2 "pixel" mode from given parameters.
 *
 * @param w Desired width.
 * @param h Desired height.
 * @param bpp Desired bits per pixel.
 * @param vbe_info Pointer to a filled @ref vbe2_info_t struct.
 * @return uint16_t Best-matching mode number.
 */
uint16_t vbe2_get_best_mode(uint16_t w, uint16_t h, uint8_t bpp, vbe2_info_t *vbe_info) {
    uint16_t *modes = vbe_info->video_modes.ptr;

    uint16_t best         = modes[0];
    int      best_pixdiff = abs_diff(320 * 200, w * h);
    int      best_bppdiff = abs_diff(8, bpp);

    vbe2_mode_info_t vbe_mode_info;
    for (int i = 0; modes[i] != 0xFFFF; i++) {
        if (!vbe2_get_mode_info(modes[i], &vbe_mode_info)) {
            continue;
        }

        if ((vbe_mode_info.attributes & 0x80) != 0x80) {
            continue;
        }

        if (vbe_mode_info.memory_model != 4 && vbe_mode_info.memory_model != 6) {
            continue;
        }

        uint16_t cur_w = vbe_mode_info.w, cur_h = vbe_mode_info.h, cur_bpp = vbe_mode_info.bpp;

        if (w == cur_w && h == cur_h && bpp == cur_bpp) {
            best = modes[i];
            break;
        }

        int pixdiff = abs_diff(cur_w * cur_h, w * h);
        int bppdiff = abs_diff(cur_bpp, bpp);
        if (best_bppdiff > pixdiff || (best_pixdiff == pixdiff && best_bppdiff > bppdiff)) {
            best         = modes[i];
            best_pixdiff = pixdiff;
            best_bppdiff = bppdiff;
        }
    }

    return best;
}

/**
 * @brief Set the VBE2 mode.
 *
 * @param mode Desired mode.
 * @return int 1 if operation was successful, 0 otherwise.
 */
int vbe2_set_mode(uint16_t mode) {
    gp_registers_t registers = {0};
    registers.ax             = 0x4f02;
    registers.bx             = mode | (1 << 14);

    int_registers_t res = real_int(0x10, registers);

    return res.gp_registers.ax != 0x004f;
}

/**
 * @brief Get and set the best-matching VBE2 "pixel" mode from given parameters.
 *
 * @param w Desired width.
 * @param h Desired height.
 * @param bpp Desired bits per pixel.
 * @param vbe_info Pointer to a filled @ref vbe2_info_t struct.
 * @param vbe_mode_info Pointer to a temporary @ref vbe2_mode_info_t struct to be used as scratchpad
 * memory.
 * @return uint16_t Pointer to the framebuffer.
 */
void *vbe2_find_and_set_mode(uint16_t w, uint16_t h, uint8_t bpp, vbe2_info_t *vbe_info) {
    if (!vbe2_get_bios_info(vbe_info)) {
        return NULL;
    }

    vbe2_mode_info_t vbe_mode_info;
    uint16_t         best = vbe2_get_best_mode(w, h, bpp, vbe_info);
    if (!vbe2_get_mode_info(best, &vbe_mode_info)) {
        return NULL;
    }

    vbe2_set_mode(best);
    return vbe_mode_info.framebuffer;
}
