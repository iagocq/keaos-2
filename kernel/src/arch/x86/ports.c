/**
 * @file ports.c
 * @brief Implementation for I/O ports manipulation on main bus.
 */

#include <x86/ports.h>

/**
 * @brief Read a byte from a port.
 *
 * @param port Port to read from.
 * @return uint8_t Byte read.
 */
uint8_t inb(uint16_t port) {
    uint8_t result;
    __asm__("in %%dx, %%al" : "=a"(result) : "d"(port));
    return result;
}

/**
 * @brief Write a byte in a port.
 *
 * @param port Port to write in.
 * @param data Byte to write.
 */
void outb(uint16_t port, uint8_t data) {
    __asm__("out %%al, %%dx" : : "a"(data), "d"(port));
}

/**
 * @brief Read a word from a port.
 *
 * @param port Port to read from.
 * @return uint16_t Word read.
 */
uint16_t inw(uint16_t port) {
    uint16_t result;
    __asm__("in %%dx, %%ax" : "=a"(result) : "d"(port));
    return result;
}

/**
 * @brief Write a word in a port.
 *
 * @param port Port to write in.
 * @param data Word to write.
 */
void outw(uint16_t port, uint16_t data) {
    __asm__("out %%ax, %%dx" : : "a"(data), "d"(port));
}

/**
 * @brief Read a dword from a port.
 *
 * @param port Port to read from.
 * @return uint32_t Dword read.
 */
uint32_t inl(uint16_t port) {
    uint32_t result;
    __asm__("in %%dx, %%eax" : "=a"(result) : "d"(port));
    return result;
}

/**
 * @brief Write a dword in a port.
 *
 * @param port Port to write in.
 * @param data Dword to write.
 */
void outl(uint16_t port, uint32_t data) {
    __asm__("out %%eax, %%dx" : : "a"(data), "d"(port));
}
