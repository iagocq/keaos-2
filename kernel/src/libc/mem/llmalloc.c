/*  This file provides a simple Linked List Memory Allocator for the kernel.
    The simplicity comes at a price: the allocator does not scale well.

    An allocation request will walk through the allocation chain, looking for
    a big enough free region to create a block in.
*/

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <math.h>
#include <mem/llmalloc.h>

/* Private declarations */
static void *  make_next_header(llmalloc_block_h_t *header,
                                size_t              new_size // bytes + sizeof(llmalloc_block_h_t)
  );
static uint8_t header_exists(llmalloc_block_h_t *header);

llmalloc_control_t control;
size_t             ll_total_bytes;

/************************************************
 *  PRIVATE FUNCTIONS                           *
 ************************************************/

static void *make_next_header(llmalloc_block_h_t *header,
                              size_t              new_size // bytes + sizeof(llmalloc_block_h_t)
) {
    size_t              block_size = sizeof(llmalloc_block_h_t);
    llmalloc_block_h_t *new_header = (void *) (((size_t) header) + new_size);

    new_header->next_block = header->next_block;
    new_header->size       = abs(header->size - (new_size - block_size));
    new_header->free       = 1;

    header->next_block = new_header;
    header->size       = new_size - block_size;

    return new_header;
}

static uint8_t header_exists(llmalloc_block_h_t *header) {
    llmalloc_block_h_t *current_header = control.first;

    while (current_header != header) {
        if (current_header == NULL) {
            return 0;
        }
        current_header = current_header->next_block;
    }

    return 1;
}

llmalloc_block_h_t *next_free_block(llmalloc_block_h_t *start, size_t to_alloc) {
    while (start != NULL) {
        if (start->free == 1 && start->size >= to_alloc)
            return start;
        start = start->next_block;
    }
    return NULL;
}

/************************************************
 *  PUBLIC FUNCTIONS                            *
 ************************************************/

/**
 * @brief Initialize llmalloc for usage.
 *
 * @param start Start address of the region reserved for allocations.
 * @param max_bytes Maximum number of bytes to allocate.
 */
void init_llmalloc(void *start, size_t max_bytes) {
    control.start_mem         = start;
    control.first             = (llmalloc_block_h_t *) start;
    control.first->next_block = NULL;
    control.first->size       = max_bytes;
    control.first->free       = 1;

    ll_total_bytes = max_bytes;
}

/**
 * @brief Allocate bytes for arbitrary usage.
 *
 * @param bytes Number of bytes to allocate.
 * @return void* Pointer to the allocated memory region, or NULL if something went wrong.
 */
void *llmalloc(size_t bytes) {
    if (control.start_mem == NULL || bytes == 0)
        return NULL;

    size_t              to_alloc       = bytes + sizeof(llmalloc_block_h_t);
    llmalloc_block_h_t *current_header = control.first;

    // Ideally, there should always be a free block for new allocations
    if (!(current_header = next_free_block(current_header, to_alloc)))
        return NULL;

    make_next_header(current_header, to_alloc);
    current_header->free = 0;
    return ((void *) current_header) + sizeof(llmalloc_block_h_t);
}

/**
 * @brief Allocate bytes in an aligned address.
 *
 * @param alignment Alignment value to align the allocated memory to.
 * @param bytes Number of bytes to allocate.
 * @return void* Pointer to the allocated memory region, or NULL if something went wrong.
 */
void *llalign(size_t alignment, size_t bytes) {
    if (control.start_mem == NULL || bytes == 0 || (alignment == 0) ||
        ((alignment & (alignment - 1)) != 0))
        return NULL;

    size_t              to_alloc       = bytes + sizeof(llmalloc_block_h_t), extra_size;
    llmalloc_block_h_t *current_header = control.first;

    while (current_header != NULL) {
        extra_size = alignment - ((size_t) current_header + sizeof(llmalloc_block_h_t)) % alignment;
        if (current_header->free == 1 && current_header->size >= to_alloc + extra_size)
            break;
        current_header = current_header->next_block;
    }

    if (!current_header)
        return NULL;

    if (extra_size >= sizeof(llmalloc_block_h_t) + sizeof(size_t)) {
        current_header = make_next_header(current_header, extra_size);
    } else {
        to_alloc += extra_size;
    }

    make_next_header(current_header, to_alloc);
    current_header->free = 0;
    return ((void *) current_header) + sizeof(llmalloc_block_h_t);
}

/**
 * @brief Free a memory region from usage.
 *
 * @param mem Pointer to the memory region, as returned by llmalloc or llrealloc.
 * @return uint8_t 1 if successful, 0 if the operation failed.
 */
uint8_t llfree(void *mem) {
    if (control.start_mem == NULL)
        return 0;

    llmalloc_block_h_t *real_header = mem - sizeof(llmalloc_block_h_t);

    if (!header_exists(real_header))
        return 0;

    // real_header (current_header->next_block) => real_header->next_block
    /* To free real_header, we set its free value to 1,
       and merge the next one if it's free
    */

    real_header->free = 1;

    if (real_header->next_block->free == 1) {
        real_header->size += real_header->next_block->size;
        real_header->next_block = real_header->next_block->next_block;
    }

    return 1;
}

// FIXME llrealloc has no effect on passed mem
/**
 * @brief Reallocate a memory region to fit a new size.
 *
 * @param mem Pointer to the memory region, as returned by llmalloc or llrealloc.
 * @param bytes New number of bytes of the memory region.
 * @return uint8_t 1 if successful, 0 if the operation failed.
 */
uint8_t llrealloc(void *mem, size_t bytes) {
    if ((control.start_mem == NULL) || (bytes == 0))
        return 0;

    size_t              block_size  = sizeof(llmalloc_block_h_t);
    llmalloc_block_h_t *real_header = mem - block_size;

    if (!header_exists(real_header))
        return 0;

    if (bytes == real_header->size)
        return 1;

    size_t new_size = bytes + block_size;
    size_t old_size = real_header->size;

    if ((new_size < real_header->size) ||
        ((real_header->next_block != NULL) &&
         (bytes - real_header->size < real_header->next_block->size) &&
         (real_header->next_block->free == 1)))
        make_next_header(real_header, new_size);
    else {
        void *new_mem = llmalloc(bytes);
        if (!new_mem)
            return 0;

        // TODO Fix possible undefined behaviour
        memcpy(new_mem, mem, old_size);
        llfree(mem);

        mem = new_mem;
    }

    return 1;
}

/**
 * @brief Get the number of free bytes, including unusable internal data.
 *
 * @return size_t Number of free bytes.
 */
size_t free_bytes() {
    if (control.start_mem == NULL)
        return 0;

    size_t free_bytes = 0;

    llmalloc_block_h_t *current_header = control.first;
    while (current_header != NULL) {
        if (current_header->free)
            free_bytes += current_header->size;
        current_header = current_header->next_block;
    }

    return free_bytes;
}

/**
 * @brief Get the number of available bytes, excluding unusable internal data.
 *
 * @return size_t Number of available bytes.
 */
size_t available_bytes() {
    if (control.start_mem == NULL)
        return 0;

    size_t available_bytes = 0;
    size_t block_size      = sizeof(llmalloc_block_h_t);

    llmalloc_block_h_t *current_header = control.first;
    while (current_header != NULL) {
        if (current_header->free)
            available_bytes += current_header->size;
        available_bytes -= block_size;
        current_header = current_header->next_block;
    }

    return available_bytes;
}
