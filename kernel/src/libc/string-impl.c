#include <stddef.h>
#include <stdint.h>

size_t strlen(const char *str) {
    size_t i = 0;
    while (*str++ != '\0')
        i++;
    return i;
}

void *memset(void *mem, int v, size_t n) {
    uint8_t *n_mem = (uint8_t *) mem;
    for (; n > 0; n--)
        n_mem[n] = v;
    return mem;
}

void *memcpy(void *restrict dest, const void *restrict src, size_t count) {
    uint8_t *      dest_u8 = dest;
    const uint8_t *src_u8  = src;
    for (size_t i = 0; i < count; i++)
        dest_u8[i] = src_u8[i];
    return dest;
}
