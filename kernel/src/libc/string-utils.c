#include <stdint.h>

char *itoa(int num, char *str, int base) {
    uint8_t neg   = 0;
    char *  start = str;

    if (num == 0) {
        str[0] = '0';
        str[1] = '\0';
        return str;
    }
    str[0] = '\0';
    str++;

    if (num < 0 && base == 10) {
        neg = 1;
        num = -num;
    }

    uint32_t unum = num;
    while (unum > 0) {
        *str++ = "0123456789abcdef"[unum % base];
        unum   = unum / base;
    }

    if (neg) {
        *str++ = '-';
    }

    char *orig = start;
    char  s;
    while (start < str) {
        s        = *(--str);
        *str     = *start;
        *start++ = s;
    }

    return orig;
}
