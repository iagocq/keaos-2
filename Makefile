CC = clang -target i386-pc-none -ffreestanding -mno-sse
LD = ld.lld
AS = nasm

iso := keaos.iso
grub_cfg := grub.cfg

OBJROOT   := $(or ${OBJROOT},obj)
OBJDEBUG   = ${OBJROOT}/debug
OBJDEFAULT = ${OBJROOT}/default

INSTALLFILE := $(realpath $(or ${INSTALLFILE},fat.o))
INSTALLDIR  := $(realpath $(or ${INSTALLDIR},fat.o.d))

all: build install run

build: build-bootloader build-kernel
install: build install-bootloader install-kernel
install-debug: build install-bootloader-debug install-kernel-debug

${OBJROOT}:
	mkdir -p ${OBJROOT}

build-bootloader:
	$(MAKE) -C iyasb build
install-bootloader: build-bootloader
	mkdir -p ${INSTALLDIR}/boot
	iyasb/iyasb ${INSTALLFILE} ${INSTALLDIR}
	sync ${INSTALLFILE}
	sync -f ${INSTALLDIR}/.

install-grub: install-kernel
	mkdir -p ${INSTALLDIR}/boot/grub
	cp ${grub_cfg} ${INSTALLDIR}/boot/grub
	grub-mkrescue -o ${iso} ${INSTALLDIR}

####
# KERNEL BUILD AND INSTALL (DEFAULT)
####
build-kernel: ${OBJDEFAULT}/guard
	@CC="${CC}" LD="${LD}" AS="${AS}" \
	OBJDIR="../${OBJDEFAULT}/kernel" \
	$(MAKE) -C kernel build --no-print-directory

install-kernel: ${OBJDEFAULT}/guard
	@CC="${CC}" LD="${LD}" AS="${AS}" \
	OBJDIR="../${OBJDEFAULT}/kernel" \
	INSTALLDIR="${INSTALLDIR}" \
	$(MAKE) -C kernel install

	mkdir -p ${INSTALLDIR}/boot
	mv ${INSTALLDIR}/kernel ${INSTALLDIR}/boot/kernel
	sync ${INSTALLDIR}/boot/kernel

####
# KERNEL BUILD AND INSTALL (DEBUG)
####
build-kernel-debug: ${OBJDEBUG}/guard
	@CC="${CC}" LD="${LD}" AS="${AS}" \
	OBJDIR="../${OBJDEBUG}/kernel" \
	DEBUG="-D DEBUG" \
	$(MAKE) -C kernel build

install-kernel-debug: ${OBJDEBUG}/guard
	@CC="${CC}" LD="${LD}" AS="${AS}" \
	OBJDIR="../${OBJDEBUG}/kernel" \
	DEBUG="-D DEBUG" \
	INSTALLDIR="${INSTALLDIR}" \
	$(MAKE) -C kernel install

	mv ${INSTALLDIR}/kernel ${INSTALLDIR}/boot/kernel
	sync ${INSTALLDIR}/boot/kernel


run:
	qemu-system-i386 \
		-drive id=disk0,file=${INSTALLFILE},format=raw,if=none \
		-device ahci,id=ahci \
		-device ide-hd,drive=disk0,bus=ahci.0
debug:
	qemu-system-i386 \
		-drive id=disk0,file=${INSTALLFILE},format=raw,if=none \
		-device ahci,id=ahci \
		-device ide-hd,drive=disk0,bus=ahci.0 \
		-s -S &
	sleep 0.5 && gdb -q -x gdb_commands
debug-wait:
	qemu-system-i386 -drive file=${INSTALLFILE},format=raw -s -S &
run-grub:
	qemu-system-x86_64 \
		-drive id=cd0,file=${iso},if=none,format=raw \
		-device ahci,id=ahci \
		-device ide-cd,drive=cd0,bus=ahci.0
debug-grub:
	qemu-system-x86_64 \
		-drive id=cd0,file=${iso},if=none,format=raw \
	    -device ahci,id=ahci \
		-device ide-cd,drive=cd0,bus=ahci.0 \
		-s -S &
	sleep 0.5 && gdb -q -x gdb_commands

clean:
	rm -rf obj

%/guard:
	@mkdir -p $(dir $@)
	@touch $@

.PHONY: all build install install-debug clean run debug debug-wait \
		build-kernel build-kernel-debug install-kernel install-kernel-debug \
		build-bootloader install-bootloader
